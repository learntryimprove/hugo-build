+++
title = "Basic Understanding & Expectation"
weight = 999003
chapter = false
pre = "<b>3. </b>"
+++
---

- In order to place your contribution well, you will need to understand the existing alignment of this project. Understanding that will help you take full advantage of project
- Few basic skills needed before you get started with contribution are listed below,
1. [Markdown](https://www.markdownguide.org/) 
2. [Gitlab Version Control System](https://docs.gitlab.com/ee/user/) 
3. [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
4. [Hugo Website building Framework](https://gohugo.io/getting-started/quick-start/)
- Project structure overview
Here’s a short reference for the purpose of each folder/file based on standard Hugo’s structure.
1. **archetypes**: this is where we can create default front matter for our content, Not used right now but may be in future.
2. **config.toml**: default configuration for project goes here. YAML and JSON are also supported.
3. **assets (not a default folder)**: anything that requires processing by Hugo Pipes (such as SCSS or images) lives here.
4. **content**: all our content (generally markdown) for pages lives here.
5. **data**: supplemental data files live, acting as a surrogate "database".Not used right now but may be in future.
6. **layouts**: all page templates for your content folder live here.
7. **static**: contains all your assets that don't need processing. Hugo will simply copy any files here to our built site without touching them.
8. **themes**: any themes you’re using can be added here.
- As an Author you are primarily expected to work under content folder with Markdown programming language.

---
Proceed with next page in this book [Working With Content](/want-to-contribute/working-with-content)