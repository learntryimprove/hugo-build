+++
title = "How To Join?"
weight = 999001
chapter = false
pre = "<b>1. </b>"
+++
---

- In order to start with contribution you need to have an account with [GitLab: The One DevOps Platform](https://gitlab.com/users/sign_in). 
- Not having a [GitLab: The One DevOps Platform](https://gitlab.com) account then create one using [Gitlab Sign-Up](https://gitlab.com/users/sign_up)
- With logged in account visit [https://gitlab.com/learntryimprove/hugo-build](https://gitlab.com/learntryimprove/hugo-build)
- Request for access using [link](https://gitlab.com/learntryimprove/hugo-build/-/project_members/request_access) or Clicking on **Request Access** Link shown below,
![Request Access](/images/want-to-contribute/request-access.png?classes=shadow&width=60pc) 
- Access can be withdrawn at any time by using [link](https://gitlab.com/learntryimprove/hugo-build/-/project_members/leave) or Clicking on **Withdraw Access Request** Link shown below,
![Withdraw Access](/images/want-to-contribute/withdraw-access.png?classes=shadow&width=60pc)


---
Proceed with next page in this Book [Getting Local Setup Up](/want-to-contribute/getting-local-setup-up)