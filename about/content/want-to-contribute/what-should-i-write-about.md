+++
title = "What should I write about?"
weight = 999005
chapter = false
pre = "<b>5. </b>"
+++
---
You can choose any topic which is related to,

- DevOps & DevSecOps
- Automation – Ansible, Puppet, Chef
- Containers – Docker, Kubernetes, OpenShift
- Infrastructure as Code – Terraform, Pulumi, Packer
- CICD & GitOps – Jenkins, GitHub Actions, GitLab CICD, Bitbucket, ArgoCD
- Cloud – AWS, GCP, Azure, DigitalOcean
- Database – InfluxDB, Elastic Stack, MySQL, MariaDB, PostgreSQL
- Monitoring - Prometheus, Grafana, Nagios

Do you have something else in mind, please propose?

---
Proceed with next page in this book [Happy contribution journey](/want-to-contribute/happy-contribution-journey)