+++
title = "Working with Content"
weight = 999004
chapter = false
pre = "<b>4. </b>"
+++
---

- [Folders and Files](#folder-and-files)
- [Header of Page](#header-of-page)
- [Footer of Page](#footer-of-page)
- [Styling pages](#styling-chapters--pages)
- [Links](#links)
---
### Folder and Files 
- All folder and files names are expected to contain only small letter alphabets and number with only allowed symbols as '-' (hyphen) and '_' (underscore)
- Each folder in **content** folder stands for a book e.g. **content/ansible**, it contains its **_index.md** page 
- Each sub-folder in **content/ansible** folder stands for a chapter for that book e.g. **content/ansible/ansible-concepts**, it contains its **_index.md** page
- Each Book folder e.g. **content/ansible**, Chapter folder e.g. **content/ansible/ansible-concepts** can contain only Markdown files with extension `.md` files   
- Commonly referred Images and related static files to be kept inside **content/images/**
- Each book folder will have its own image folder, e.g. for placing an image for book **content/ansible** folder name is **content/images/ansible**

### Header of Page
- Start of Page - This is as per syntax reference of Hugo documentation 
> +++
- Title of Page - Should be brief description of the content of Page 
> title = "Working with Content"
- Wight - Weight of page based on which indexing will be carried out by Hugo, more details of how Weight planned in this project can be understood [here](/want-to-contribute/page-weight-logic)
> weight = 999004
- Chapter - This will be false for all non index pages, can be aligned as per need though
> chapter = false
- Pre - This is a Prefix displayed in sidebar index on site for particular page. In this project its considered as sequence number in that Book or Chapter
> pre = "<b>4. </b>"
- End of Page - This is as per syntax reference of Hugo documentation 
> +++

### Footer of Pages
- Link to next page
> Proceed with next page in this chapter `[Next Page Label](#footer-of-pages)`

### Styling Chapters / Pages

- Chapter title
```
# This is a chapter title
```
- Chapter partition/section divider
```
---
## A chapter part title is preceded by an horizontal line
```
- Chapter subpart
```
### A level 1 subpart
#### A level 2 subpart
```
- Images
```
![An image should have an alt text](use/a/relative.link)
```
- Code
```
An `inline code block` goes like this
```

### Links
- All Internal Page links should be without prefix/context e.g. for Ansible book's index page folder link would be `/ansible`
- All External Page links to DEV documentation should have actual url as `https://devops.learntryimprovise.info/`
- All External Page links to DEV documentation should have actual url as `https://dev.learntryimprovise.info/`
- All External Page links to DEV documentation should have actual url as `https://qa.learntryimprovise.info/`
- All Internal Image references should have prefix `/` e.g. for work in progress image link would be `![Work In progress](/images/work-in-progress-v2.gif 'Work In progress')`

---
Proceed with next page in this Book [Understanding Page Weight Logic](/want-to-contribute/page-weight-logic) 