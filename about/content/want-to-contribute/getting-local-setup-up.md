+++
title = "Getting Local Setup Up"
weight = 999002
chapter = false
pre = "<b>2. </b>"
+++
---

### 1. Getting local setup in place  
- Clone the master branch of this Blog repo maintained in gitlab
- Install Hugo
- Reference link for validating below steps as per latest release of Hugo 
**for Linux**
```
$ mkdir .hugoportable && cd .hugoportable
$ wget https://github.com/gohugoio/hugo/releases/download/v0.104.0/hugo_extended_0.104.0_linux-amd64.tar.gz 
$ tar -xvf hugo_extended_0.104.0_linux-amd64.tar.gz 
$ ./hugo version
```
**for Windows (with Git bash)**
```
$ mkdir .hugoportable && cd .hugoportable
$ wget https://github.com/gohugoio/hugo/releases/download/v0.104.0/hugo_extended_0.104.0_windows-amd64.zip
$ ./hugo.exe version
```
### 2. Initializing Themes 
Necessary themes used in this Hugo project can be placed using below commands.
```
$ mkdir themes
$ git clone https://github.com/matcornic/hugo-theme-learn.git -b master themes/learn
```

### 3. Starting and Accessing Hugo Server

- Start the server locally
**for Linux**
```
$ ./.hugoportable/hugo server --bind 0.0.0.0
```
**for Windows (with Git bash)**
```
$ ./.hugoportable/hugo.exe server --bind 0.0.0.0
```
- Visit [http://localhost:1313/want-to-contribute/](http://localhost:1313/want-to-contribute/)

---
Proceed with next page in this book [Basic Understanding and Expectation of this Project](/want-to-contribute/basic-understanding-and-expectation)