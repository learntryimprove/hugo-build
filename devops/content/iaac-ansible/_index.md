+++
title = "IaaC & Ansible"
weight = 6001
chapter = false
+++

---
### Ansible is Simple IT Automation
Ansible is an open source community project sponsored by Red Hat, it's the simplest way to automate IT.
---
- [Learning Concepts](ansible-concepts)
- [Understanding Inventory and Playbooks](ansible-inventory-playbooks)
- [Getting started with Roles and Vault](ansible-roles-vault)
- [Ansible Automation Platform (formerly known as Ansible Tower)](ansible-tower)
- [Tutorials](ansible-tutorials)
- [Case Studies](ansible-casestudies)