+++
title = "Git Basics"
weight = 4100
chapter = false
pre = "<b>1. </b>"
+++

- Introduction to Git
    - Getting Started with Software Development
    - What is Version Control System (VCS)/Source Control Management (SCM)?
    - Need for VCS/SCM
    - Benefits of VCS/SCM
    - What is Git?
    - History of Git
    - Alternatives for Git
- Key Components in Git
    - Git Repository
    - Git Branch
    - Git Tag
    - Git Service Provider
- Overview of Git
    - Architecture of Git-managed Software Development
    - Centralized Distributed
    - Git vs Subversion
- FAQ on Git
- Setup Git
    - Install Git
    - Significance of Git Programs
    - Understanding Git System Paths
- Configure Git
    - Understanding Git Configurations
    - Apply Global Level Configurations in Git
- Do's and Dont's with Git Repository
