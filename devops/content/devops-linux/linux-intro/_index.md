+++
title = "Linux Intro"
weight = 1300
chapter = false
pre = "<b>3. </b>"
+++

### Overview of Linux
- Architecture of Linux
- User Space
- Kernel Space
- Linux vs Windows
- FAQ on Linux
- Prerequisites for Linux