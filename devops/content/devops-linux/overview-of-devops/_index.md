+++
title = "Overview of Devops"
weight = 1100
chapter = false
pre = "<b>1. </b>"
+++

### DevOps Introduction
- What is DevOps?
- Why DevOps?
- Where DevOps is useful
- History of DevOps
- DevOps and Software Development Life Cycle
- DevOps main objectives
- DevOps on the Cloud
- Prerequisites for DevOps
- Frameworks for Devops