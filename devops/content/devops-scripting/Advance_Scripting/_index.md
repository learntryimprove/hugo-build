+++
title = "Advance_Scripting"
weight = 3200
chapter = false
pre = "<b>2. </b>"
+++

- Next Level of commands
    - Working with find command 
    - Working with sed command 
    - Working with awk command 
    - Working with regular expression 
- Expect scripts
    - What is expect? 
    - Where to Use Expect?
    - Working with expect scripting
- SUDO in scripts
    - Requirements for using sudo in bash scripts
    - Inbuilt variables for SUDO
    - Working with sudo in shell scripts
- shift & set/unset operator
    - Working with shift
    - Working with getopts
    - Working with set
- Integrating bash with Other tools
    -  Integration with except command based scripting
    -  Integration with ansible scripting
    -  Integration with CGI scripting
    -  Integration with web page scripting (php/python/perl)
