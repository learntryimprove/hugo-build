+++
title = "Components"
draft = false
weight = 3
+++

### Organization's GITLAB Structure 

If you look at the overall organization structure within GitLab from a project management viewpoint, the group, subgroup, and project relationship could be thought of together as portfolio or program management.

Explore the diagram below to learn more about how each element relates to each other in the hierarchy.

![Organization Structure](/images/gitlab/gitlab-organization-structure.png "Organization Structure Icon")

### Epics

Epics let you manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones.

Epics allow you to group related issues and epics together. This is useful if you want better visibility of related issues. For example, you might want to see all of the issues in a project in a roadmap and generate a Gantt chart for your program or portfolio.

"Arriving at one goal is the starting point of another"- John Dewey

### The Starting Point for our GitLab Workflow

Issues are the basic building blocks of planning development work and are used to collaborate on ideas and planning work in GitLab. 

You should create a new issue whenever you are developing a new feature to be implemented in one of your applications. You can also create issues for:

1. Discussing the implementation of a new idea
2. Managing an incident
3. Asking questions 
4. Reporting bugs

At Eigenlytics our best practice is to always start a discussion with an issue.

`Single Source of Truth`

When you create an issue in GitLab, you can view multiple DevOps Lifecycle Stage properties, such as the related epics, merge requests, milestone dates, boards, roadmaps, labels, and more- it's all connected!

In the image below, you can see how Issues relate to almost every other GitLab component and how Issues help tie everything together. 

![Issue Relation With Other Components](/images/gitlab/issue-relation-with-other-components.png "Issue Relation With Other Components Icon")


### Example GitLab Issue Flow

`Introduction`

Let’s walk through the location and anatomy of an issue and how to use it to plan projects. Each Project may have it different version of issue flow but overall terminoloty remains same across projects.

`Step 1` - Create and Discuss a New Issue

1. Team creates an issue
2. Team applies “Discussion” label
3. Team discusses using Comments

`Step 2` - Code Creation

1. Backend team starts work and developer starts writing code 
2. Developer assigns issue to themselves
3. Developer adds “Working on” label

`Step 3` - Commit and Merge Request

1. Developer creates Commits
2. Developer pushes commits to a feature-branch
3. Developer creates a Merge Request (MR)
4. Backend team changes labels to “Frontend”

`Step 4` - Deploy to Staging

1. Frontend developer starts working on issue
2. Developer assigns issue to themselves
3. Developer adds “Working on” label 
4. Team reviews and refines code
5. Team stages code
6. Team changes label to “Staging” 
7. After successful implementation team changes label to “Ready”

`Step 5` - Ready

1. Technical documentation team adds "Docs" label
2. Marketing team adds “Marketing" label
3. Technical team removes "Docs" label when done
4. When done, the marketing team removes “Marketing” label and adds “Production” label

`Step 6` - Deploy to Production

1. Release team merges MR and deploys feature to production 
2. Release team closes issue

`Summary`

This example shows how each change, no matter how small, starts with an issue. Something as small as a discussion can lead to larger beneficial changes in the organization. 


### At Eigenlytics, everyone can contribute! 

One of the best ways to get yourself familiar with the GitLab platform is to take a look at how we use GitLab!


### Exercise 

[LAB 01](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab1.html)