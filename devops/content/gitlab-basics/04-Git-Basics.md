+++
title = "Basics"
draft = false
weight = 4
+++

### What does the industry say about Git? 

"Git is amazingly fast, it’s very efficient with large projects, and it has an incredible branching system for non-linear development" - Scott Chacon and Ben Straub; authors of Pro Git

Why is Git so popular?

![Why is Git so popular](/images/gitlab/why-git-popular.png "Why is Git so popular Icon")


### What is Git?

Git is a free and open source distributed version control system(VCS) designed to handle everything from small to large projects with speed and efficiency. GitLab is a Git-based fully integrated platform for software development.

`Characteristics of Git`
1. Distributed- Does not require a constant connection to a central server.
2. Adaptive- Git's branching model can adapt to fit the workflow of almost any team.
3. Fast & Reliable- Git branches are sparse, allowing for change-only tracking.
4. Flexible- Git can be command line or GUI driven, allowing everyone to contribute easily.

### Traditional Developer Process

`DEVELOPERS`

1. Developers have access to the entire code base
2. All files are stored
3. Operations are performed directly on the repository

`CODE CHANGES`

Since  changes are made to a central repository, other developers are not aware of code changes a developer makes until they merge it to the central repository.

`CENTRAL REPOSITORY`

1. Developers work using local copy of the repo 
2. Then check in their changes and resolve any possible conflicts


### Git Term Definitions 

There are several terms that are used when learning how Git. Click on each card to learn how the term is defined. 

1. Branch - A branch is an independent line of development. 
2. Tag - Mark a specific point in time on a branch.
3. Checkout - Get a specific branch to start making your changes.
4. Commit - Add changes you've made to the repository.
5. Push - Send changes to a remote directory. 
6. Workspace - Directory where you store the repository on your computer.

### Why is Git so popular?

`Central Repository vs. Remote Repository`

![Central Repository vs. Remote Repository](/images/gitlab/central-vs-remote-repo.png "Central Repository vs. Remote Repository Icon")


Terms used when working with Git locally

1. Untracked Files – New files that git has not been told to track previously
2. Working Area – Files that has been modified but not committed
3. Staging Area – Modified/Added files that are marked to go into the next commit
4. Local Repo – Local copy of upstream repo
5. Remote/Upstream Repo – Hosted repository on a shared server (Gitlab)


`Centralized vs. Distributed`

Let's take a look at the various advantages of using a distributed VCS over a centralized VCS.

`DATA LOSS` - It is safer for data loss:

1. Each developer has a clone from the repo
2. Even a CI server has a trustable source of backup

`SPEED` - It is faster:

1. Most of the operations doesn’t require network
2. It's less bureaucratic- you can create branches, commits, and merges without accessing the remote repository

`COLLABORATION` - More collaborative

1. You can have several remote repositories
2. Allows you collaborate with different developers without changing the official repository (using forks for example)


### Basic Git Workflow with GitLab

GitLab is based on Git and provides your team with a central repository. You use git locally on your own system to create and update code, share your changes with your team, and see your team’s changes by using git commands to push, fetch, and merge work.

![Basic Git Workflow GitLab](/images/gitlab/basic-git-workflow-gitlab.png "Basic Git Workflow GitLab Icon")


### GitLab Cheat Sheet 

GitLab maintains a cheat sheet of git commands for easy access. This cheat sheet can be accessed using the below link: https://about.gitlab.com/images/press/git-cheat-sheet.pdf 


### Exercise 

[LAB 02](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab2.html)