+++
title = "VPC - Flow Logs"
weight = 4
chapter = false
+++
---
- VPC Flow Logs is a feature that enables you to capture information about the IP traffic going to and from network interfaces in your VPC. 
- Flow logs can help you with a number of tasks, such as:
    - Diagnosing overly restrictive security group rules
    - Monitoring the traffic that is reaching your instance
    - Determining the direction of the traffic to and from the network interfaces
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html)