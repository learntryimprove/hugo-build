+++
title = "Virtual Private Cloud - VPC"
weight = 3
chapter = false
+++
---
- **Amazon Virtual Private Cloud (Amazon VPC)** enables you to define a virtual network in your own logically isolated area within the AWS cloud, known as a virtual private cloud or VPC.
- Few concepts are,
    - VPC basics
    - VPC CIDR blocks
    - Work with VPCs
    - Default VPCs
    - DHCP option sets in Amazon VPC
    - DNS attributes for your VPC
    - Network Address Usage for your VPC
    - Share your VPC with other accounts
    - Extend a VPC to a Local Zone, Wavelength Zone, or Outpost
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/configure-your-vpc.html)
- [Connect subnets to the internet using an internet gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html)
- [Access the internet from a private subnet](https://docs.aws.amazon.com/vpc/latest/userguide/nat-gateway-scenarios.html#public-nat-internet-access)