+++
title = "EC2 - EBS"
weight = 6
chapter = false
+++
---
- **Amazon Elastic Block Store (Amazon EBS)** provides block level storage volumes for use with EC2 instances. EBS volumes behave like raw, unformatted block devices.
- Basics
    - Amazon EBS snapshots
    - Amazon EBS encryption
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html)