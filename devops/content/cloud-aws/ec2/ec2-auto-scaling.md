+++
title = "EC2 - Auto Scaling"
weight = 7
chapter = false
+++
---
- **Amazon EC2 Auto Scaling** helps you ensure that you have the correct number of Amazon EC2 instances available to handle the load for your application. You create collections of EC2 instances, called Auto Scaling groups.
- The following table describes the key components of Amazon EC2 Auto Scaling.
    - Groups (Auto Scaling Groups - ASG)
    - Configuration templates (Launch Configuration - LC)
    - Scaling options
- When you use Amazon EC2 Auto Scaling, your applications gain the following benefits:
    - Better fault tolerance 
    - Better availability
    - Better cost management (e.g. Spot Instance Usage)
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/autoscaling/ec2/userguide/what-is-amazon-ec2-auto-scaling.html)
- [Auto Scaling Benefits](https://docs.aws.amazon.com/autoscaling/ec2/userguide/auto-scaling-benefits.html)