+++
title = "Support"
weight = 20
chapter = false
+++
---
- **AWS Support** offers a range of plans that provide access to tools and expertise that support the success and operational health of your AWS solutions. All support plans provide 24/7 access to customer service, AWS documentation, technical papers, and support forums. For technical support and more resources to plan, deploy, and improve your AWS environment, you can choose a support plan for your AWS use case.
- Some concepts can be, 
    - Creating support cases and case management
    - Creating a service quota increase
    - Updating, resolving, and reopening your case
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/awssupport/latest/user/getting-started.html)