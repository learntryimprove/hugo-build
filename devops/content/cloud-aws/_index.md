+++
title = "Cloud & AWS"
weight = 2001
chapter = false
+++

---
In order to learn AWS cloud solution offerings, its recommended to follow below sequence, 
- [Regions and Availability Zones](./regions-availability-zones)
- [Tags](./tags)
- [Service Quotas](./service-quotas)
- [Support](./support)
- [VPC](./vpc/)
- [EC2](./ec2/)
- [Cloudformation](./cloudformation)
---
Bonus pages for some learnings that we had from our hands-on experiences,
- [Concepts](./concepts)