+++
title = "Regions & Availability Zones"
weight = 1
chapter = false
+++
---
- Each Region is a separate geographic area.
- Availability Zones are multiple, isolated locations within each Region.
- Various services of AWS are made available based on regions, few may not be available in each Region.

---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html)