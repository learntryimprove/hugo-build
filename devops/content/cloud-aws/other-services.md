+++
title = "Other Services"
weight = 20
chapter = false
+++
---
below are list of next services to be documented,

- [AWS Data Pipeline](https://docs.aws.amazon.com/datapipeline/latest/DeveloperGuide/what-is-datapipeline.html)
- [Site-to-Site VPN](https://docs.aws.amazon.com/vpn/latest/s2svpn/VPC_VPN.html)
- [CloudFront](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Introduction.html)
- [Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)
- [Step Function](https://docs.aws.amazon.com/step-functions/latest/dg/welcome.html)
- [ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
- [SQS](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html)
- [SNS](https://docs.aws.amazon.com/sns/latest/dg/welcome.html)
- [Savings Plans](https://docs.aws.amazon.com/savingsplans/latest/userguide/what-is-savings-plans.html)
- [API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html)
- [Command Line](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
- [CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html)
- [Textract](https://docs.aws.amazon.com/textract/latest/dg/what-is.html)
- [Certificate Manager](https://docs.aws.amazon.com/acm/latest/userguide/acm-overview.html)
- [Shield](https://docs.aws.amazon.com/waf/latest/developerguide/shield-chapter.html)
- [WAF](https://docs.aws.amazon.com/waf/latest/developerguide/waf-chapter.html)
- [Secret Manager](https://docs.aws.amazon.com/secretsmanager/latest/userguide/intro.html)
- [Network Firewall](https://docs.aws.amazon.com/network-firewall/latest/developerguide/what-is-aws-network-firewall.html)
- [Guard Duty](https://docs.aws.amazon.com/guardduty/latest/ug/what-is-guardduty.html)
- [IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
- [Elastic Disaster Recovery](https://docs.aws.amazon.com/drs/latest/userguide/what-is-drs.html)
- [EKS](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html)
- [Lambda](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
- [RDS](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Welcome.html)