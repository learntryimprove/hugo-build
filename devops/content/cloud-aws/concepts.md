+++
title = "Concepts"
weight = 20
chapter = false
+++
---
1. Basic difference between Security Groups and Network ACLs

| Security group                                                    |	Network ACL                 |
|------------------------------------------------------------------ |------------------------------ |
| Operates at the instance level	                                | Operates at the subnet level  |
| Applies to an instance only if it is associated with the instance	| Applies to all instances deployed in the associated subnet (providing an additional layer of defense if security group rules are too permissive) |
| Supports allow rules only	                                        | Supports allow rules and deny rules |
| We evaluate all rules before deciding whether to allow traffic	| We evaluate rules in order, starting with the lowest numbered rule, when deciding whether to allow traffic |
| **Stateful**: Return traffic is allowed, regardless of the rules	| **Stateless**: Return traffic must be explicitly allowed by rules |
---
2. [AWS Calculator](https://calculator.aws/#/) is the best place to get cost predictions
---