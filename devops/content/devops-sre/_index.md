+++
title = "DevOps & SRE"
weight = 1004
chapter = false
+++

---
### 1. Welcome!
- Welcome! 
### 2. Introduction to DevOps/SRE
- Introduction 
- DevOps and SRE 
### 3. Cloud
- Introduction 
- Cloud 
### 4. Containers
- Introduction 
- Containers 
### 5. Infrastructure as a Code (IaaC)
- Introduction 
- Infrastructure as Code 
### 6. Continuous Integration/Continuous Delivery (CI/CD)
- Introduction 
- Continuous Integration/Continuous Delivery (CI/CD) 
### 7. Observability
- Introduction 
- Observability 
### 8. Site Reliability Engineering (SRE)
- Introduction 
- SRE

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home