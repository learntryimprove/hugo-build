+++
title = "Continuous Integration/Continuous Delivery (CI/CD)"
weight = 6
chapter = false
+++

---
### Introduction to CI/CD
---
### CI/CD - Why Should You Bother?
---
### What Is Continuous Integration?
---
### Continuous Deployment and Continuous Delivery
---
### CI/CD Principles and Practices
---
### Understanding Release Strategies
---
### Demo: Jenkins Pipeline for a Maven Project
---
### Demo: Pipeline as a Code
---
### Demo: Spinnaker Deployment Pipeline
---
### GitOps as a Deployment Tool

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home