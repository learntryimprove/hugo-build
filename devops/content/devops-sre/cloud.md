+++
title = "Cloud"
weight = 2
chapter = false
+++

---
### Cloud - Why Should You Bother?
---
### A Brief History of Cloud
---
### Types of Cloud
---
### Cloud Deployment Models
---
### Major Cloud Services
---
### Demos: Building Scalable, High Available Infrastructure with AWS
---
### AWS Walk-through
---
### Creating a Secure, Isolated Network on Cloud with VPC
---
### Going Live with a Web Application with EC2
---
### AWS Managed Services - Launching a Database with RDS
---
### Setting Up Availability and Scalability with AWS Autoscaling
---
### Testing Fault Tolerance with Autoscaling Groups
---
### Infrastructure Automation with CloudFormation

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home