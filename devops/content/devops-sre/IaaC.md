+++
title = "Infrastructure as a Code (IaaC)"
weight = 5
chapter = false
+++

---
### Introduction to Infrastructure as a Code
---
### Infrastructure as a Code - Why Should You Bother?
---
### 5 Categories of Infrastructure as a Code Tools
---
### 5 Fundamental Features of an IaaC Tool
---
### Introduction to Vagrant
- https://youtu.be/wlogPKBEuUM
---
### Demo: Using Vagrant for VM Provisioning
---
### Demo: Using Docker Compose to Launch Containers
---
### Introduction to Ansible
---
### Demo: Using Ansible to Configure Web Servers
---
### Demo: Using Terraform to Automatically Provision AWS Infrastructure

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home