+++
title = "What is Containers"
weight = 8104
chapter = false
pre = "<b>1.4 </b>"
+++
---
- Containers abstract applications from the environment in which they run by providing a logical packaging mechanism. But, what are the benefits of this abstraction? Well, containers allow us to deploy applications in any environment easily and consistently.
- We can develop an application on our local desktop, containerize it, and deploy it on a public cloud with confidence.
- The concept is not very different from virtual machines, but how containers achieve it is quite different. Virtual machines have been around far longer than containers, at least in the popular space.
- If we recall, virtual machines allow us to run multiple guest operating systems on top of the host operating system with the help of a virtual machine monitor like a hypervisor.
- Both virtual machines and containers virtualize access to underlying hardware like CPU, memory, storage, and network. But virtual machines are costly to create and maintain if we compare them to containers:
- As we can see in the image above, containers virtualize at the level of the operating system instead of virtualizing the hardware stack. Multiple containers share the same operating system kernel.
- This makes containers more lightweight compared to virtual machines. Consequently, containers start much faster and use far fewer hardware resources.

![Why is Docker?](/images/docker-containers/what-is-docker.png)
---
Proceed with next page in this Book [Evolution of Containers](/docker-containers/docker-overview/evolution-of-containers)
