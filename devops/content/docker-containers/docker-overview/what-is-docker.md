+++
title = "What is Docker"
weight = 8107
chapter = false
pre = "<b>1.7 </b>"
+++
---
### What is Docker ? 
- Docker is an open source containerization platform. 
- It enables developers to package applications into containers—standardized executable components combining application source code with the operating system (OS) libraries and dependencies required to run that code in any environment. Containers simplify delivery of distributed applications, and have become increasingly popular as organizations shift to cloud-native development and hybrid multicloud environments.
- Developers can create containers without Docker, but the platform makes it easier, simpler, and safer to build, deploy and manage containers. 
- Docker is essentially a toolkit that enables developers to build, deploy, run, update, and stop containers using simple commands and work-saving automation through a single API.
- Docker also refers to [Docker, Inc.](https://www.docker.com/company), the company that sells the commercial version of Docker, and to the [Docker open source project](https://github.com/docker), to which Docker, Inc. and many other organizations and individuals contribute.
- A more formal definition is offered by **Jabbari et al.**:"DevOps is a development methodology aimed at bridging the gap between Development and Operations, emphasizing communication and collaboration, continuous integration, quality assurance and delivery with automated deployment utilizing a set of development practices".

![Why Docker?](/images/docker-containers/why-docker.png)


### How Docker works ?
- Docker works by providing a standard way to run your code. Docker is an operating system for containers. 
- Similar to how a virtual machine virtualizes (removes the need to directly manage) server hardware, containers virtualize the operating system of a server. Docker is installed on each server and provides simple commands you can use to build, start, or stop containers.

### Why use Docker ?
- Using Docker lets you ship code faster, standardize application operations, seamlessly move code, and save money by improving resource utilization. With Docker, you get a single object that can reliably run anywhere. 
- Docker's simple and straightforward syntax gives you full control. Wide adoption means there's a robust ecosystem of tools and off-the-shelf applications that are ready to use with Docker.
> 1. Ship More Software Faster - Docker users on average ship software 7x more frequently than non-Docker users. Docker enables you to ship isolated services as often as needed.
> 2. Standardize Operations - Small containerized applications make it easy to deploy, identify issues, and roll back for remediation.
> 3. Seamlessly Move - Docker-based applications can be seamlessly moved from local development machines to production deployments on AWS.
> 4. Save Money - Docker containers make it easier to run more code on each server, improving your utilization and saving you money.

### When to use Docker ?
- You can use Docker containers as a core building block creating modern applications and platforms. 
- Docker makes it easy to build and run distributed microservices architectures, deploy your code with standardized continuous integration and delivery pipelines, build highly-scalable data processing systems, and create fully-managed platforms for your developers. 
> 1. Microservices - Build and scale distributed application architectures by taking advantage of standardized code deployments using Docker containers.
> 2. Continuous Integration & Delivery - Accelerate application delivery by standardizing environments and removing conflicts between language stacks and versions.
> 3. Data Processing - Provide big data processing as a service. Package data and analytics packages into portable containers that can be executed by non-technical users.
> 4. Containers as a Service - Build and ship distributed applications with content and infrastructure that is IT-managed and secured.

### Docker Components
below is a brief diagrammatic representation of various components of docker

![Docker Components](/images/docker-containers/docker-components.png)


### References:
1. https://aws.amazon.com/docker/
2. https://www.ibm.com/in-en/cloud/learn/docker
---
Proceed with next page in this Book [History of Docker](/docker-containers/docker-overview/history-of-docker)