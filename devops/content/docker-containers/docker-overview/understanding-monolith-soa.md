+++
title = "Understanding Monolith & SOA"
weight = 8102
chapter = false
pre = "<b>1.2 </b>"
+++
---
- [Monolith](#monolith)
- [Service Oriented Architecture (SOA)](#service-oriented-architecture-soa)
---
### Monolith 
- The monolithic approach is a default model for creating a software application. Still, its trend is going down because building a monolithic application poses a number of challenges associated with handling a huge code base, adopting a new technology, scaling, deployment, implementing new changes and others.
- The monolithic architecture is considered to be a traditional way of building applications. 
- A monolithic application is built as a single and indivisible unit. Usually, such a solution comprises a client-side user interface, a server side-application, and a database. It is unified and all the functions are managed and served in one place.

![Monolith](/images/docker-containers/monolith.jpg)
[Source: https://www.n-ix.com/microservices-vs-monolith-which-architecture-best-choice-your-business/](https://www.n-ix.com/microservices-vs-monolith-which-architecture-best-choice-your-business/)

- Normally, monolithic applications have one large code base and lack modularity. If developers want to update or change something, they access the same code base. So, they make changes in the whole stack at once.
- Strengths of the Monolithic Architecture
> 1. **Less cross-cutting concerns**. Cross-cutting concerns are the concerns that affect the whole application such as logging, handling, caching, and performance monitoring. 
> 2. **Easier debugging and testing**. In contrast to the microservices architecture, monolithic applications are much easier to debug and test. 
> 3. **Simple to deploy**. Another advantage associated with the simplicity of monolithic apps is easier deployment. 
> 4. **Simple to develop**. 
- Weaknesses of the Monolithic Architecture
> 1. **Understanding**. When a monolithic application scales up, it becomes too complicated to understand. Also, a complex system of code within one application is hard to manage.
> 2. **Making changes**. It is harder to implement changes in such a large and complex application with highly tight coupling. Any code change affects the whole system so it has to be thoroughly coordinated. This makes the overall development process much longer.
> 3. **Scalability**. You cannot scale components independently, only the whole application.
> 4. **New technology barriers**. It is extremely problematic to apply a new technology in a monolithic application because then the entire application has to be rewritten.

![Monolith vs Microservices](/images/docker-containers/monolith-vs-microservices.jpg)
[Source: https://www.n-ix.com/microservices-vs-monolith-which-architecture-best-choice-your-business/](https://www.n-ix.com/microservices-vs-monolith-which-architecture-best-choice-your-business/)


### Service Oriented Architecture (SOA) 
- What do containers have to do with **SOA**? First, we need to explain what **SOA** means.
- **SOA** is quite a bit old word in the industry but you should not forget the fact SOA is the most mature style if software designing even the Microservices are out there for sometimes.
- Broadly defined, **SOA** is an approach to application design that breaks an application down into discrete parts. Those parts are generally distributed across a system and communicate with each other over the network or through APIs.
- Traditionally, **SOA** was implemented by writing sockets or plug-ins that allowed the different components of an application to exchange information. The services over which each part of the application communicated were then orchestrated in some way through a management layer.
- **SOA** was designed to address some of the limitations had in a monolithic architecture. In **SOA**, a set of similar functions are grouped together to introduce a concept called "service".
- On the other hand, **Microservices**, aka microservices architecture(MSA), is an architectural style that structures a software application as a collection of small autonomous and independent services that are running in its own process, developed and deployed independently to modeled around a business domain.
- For some organizations, SOA architecture is a steppingstone to replace the monolith, providing a more flexible and agile environment. SOA services can be developed and utilized in a large environment, but they do not address specific needs of individual businesses that wish to address business processes within their purview. 

![SOA vs MicroServices](/images/docker-containers/soa-vs-microservices.png)


### References: 
1. https://www.ibm.com/in-en/cloud/learn/soa#toc-soa-vs-mic-BjTfju28
2. https://www.ibm.com/cloud/devops/soa-vs-microservices
---
Proceed with next page in this Book [Understanding Microservices](/docker-containers/docker-overview/understanding-microservices)
