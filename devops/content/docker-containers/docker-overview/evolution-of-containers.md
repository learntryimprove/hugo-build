+++
title = "Evolution of Containers"
weight = 8105
chapter = false
pre = "<b>1.5 </b>"
+++
---
- 1979 :  Unix V7
- 2000 :  FreeBSD Jails
- 2001 :  Linux VServer
- 2004 :  Solaris Containers
- 2005 :  Open VZ (Open Virtuzzo)
- 2006 :  Process Containers
- 2008 :  LXC
- 2011 :  Warden
- 2013 :  LMCTFY
- 2013 :  Docker
- 2016 :  The Importance of Container Security Is Revealed
- 2017 :  Container Tools Become Mature
- 2018 :  The Gold Standard
- 2019 :  A Shifting Landscape

### Reference :
- https://devops.aquasec.com/a-brief-history-of-containers-from-1970s-chroot-to-docker-2016
- https://www.baeldung.com/linux/docker-containers-evolution
---
Proceed with next page in this Book [Benefits of Containers](/docker-containers/docker-overview/benefits-of-containers)
