+++
title = "Introduction to Docker"
weight = 8101
chapter = false
pre = "<b>1.1 </b>"
+++
---

### Introduction

- **Docker** is a centralized platform for packaging, deploying, and running applications. 
- Before Docker, many users face the problem that a particular code is running in the developer's system but not in the user's system. Shipping applications to the intended users were not only a complex task but tedious as well. In simple words, Two of the main reasons why the applications do not perform the same way in all the machines are:
>  1. Applications version discrepancy between different machines
>  2. Inconsistent environment variables across machines
- So, the main reason to develop docker is to help developers to develop applications easily, ship them into containers, and can be deployed anywhere. 
- Docker was firstly released in March 2013. It is used in the Deployment stage of the software development life cycle that's why it can efficiently resolve issues related to the application deployment.

![Docker Was Born](/images/docker-containers/docker-was-born.png)
[Source: https://dzone.com/articles/docker-commands-you-should-know-as-a-beginner](https://dzone.com/articles/docker-commands-you-should-know-as-a-beginner)

---
Proceed with next page in this Book [Understanding Monolith and SOA](/docker-containers/docker-overview/understanding-monolith-soa)