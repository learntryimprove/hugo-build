+++
title = "Understanding Microservices"
weight = 8103
chapter = false
pre = "<b>1.3 </b>"
+++
---

---
- The main distinction between the two approaches comes down to scope. To put it simply, service-oriented architecture (SOA) has an enterprise scope, while the microservices architecture has an application scope.
- Here are a few use cases where this distinction comes into play:
### Reuse
- In SOA, reusability of integrations is the primary goal, and at an enterprise level, striving for some level of reuse is essential. Reusability and component sharing in an SOA architecture increases scalability and efficiency.
- In microservices architecture, creating a microservices component that is reused at runtime throughout an application results in dependencies that reduce agility and resilience. Microservices components generally prefer to reuse code by copying and accepting data duplication to help improve decoupling.
### Synchronous calls
- The reusable services in SOA are available across the enterprise using predominantly synchronous protocols like RESTful APIs.
- However, within a microservice application, synchronous calls introduce real-time dependencies, resulting in a loss of resilience. These dependencies may also cause latency, which impacts performance. Within a microservices application, interaction patterns based on asynchronous communication are preferred, such as event sourcing, in which a publish/subscribe model is used to enable a microservices component to remain up to date on changes happening to the data in another component.
### Data duplication
- A clear aim of providing services in an SOA is for all applications to synchronously obtain and alter data directly at its primary source, which reduces the need to maintain complex data synchronization patterns.
- In microservices applications, ideally, each microservice has local access to all the data it needs to ensure its independence from other microservices — and indeed from other applications — even if this means some duplication of data in other systems. Of course, this duplication adds complexity, so it must be balanced against the gains in agility and performance, but this is accepted as a reality of microservices design.

### Communication
- In a microservices architecture, each service is developed independently, with its own communication protocol. With SOA, each service must share a common communication mechanism called an enterprise service bus (ESB). SOA manages and coordinates the services it delivers through the ESB. However, the ESB can become a single point of failure for the whole enterprise, and if a single service slows down, the entire system can be affected.

### Interoperability
- In the interest of keeping things simple, microservices use lightweight messaging protocols like HTTP/REST (Representational State Transfers) and JMS (Java Messaging Service). SOAs are more open to heterogeneous messaging protocols such as SOAP (Simple Object Access Protocol), AMQP (Advanced Messaging Queuing Protocol) and MSMQ (Microsoft Messaging Queuing).

### Service granularity
- Microservices architectures are made up of highly specialized services, each of which is designed to do one thing very well. The services that make up SOAs, on the other hand, can range from small, specialized services to enterprise-wide services.

## Speed
- By leveraging the advantages of sharing a common architecture, SOAs simplify development and troubleshooting. However, this also tends to make SOAs operate more slowly than microservices architectures, which minimize sharing in favor of duplication.

### Governance
- The nature of SOA, involving shared resources, enable the implementation of common data governance standards across all services. The independent nature of microservices does not enable consistent data governance. This provides greater flexibility for each service, which can encourage greater collaboration across the organization.

### Storage
- SOA and microservices also differ in terms of how storage resources are allocated. SOA architecture typically includes a single data storage layer shared by all services within a given application, whereas microservices will dedicate a server or database for data storage for any service that needs it.


DevOps can be used to help an organization transition from SOA architecture to microservices to address specific needs.

---
Proceed with next page in this Book [What is Containers](/docker-containers/docker-overview/what-is-containers)
