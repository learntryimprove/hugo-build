+++
title = "Docker Compose"
weight = 8600
chapter = false
pre = "<b>6. </b>"
+++

- Docker Compose
    - Understanding YAML
    - What is the Purpose of Docker Compose?
    - Install Docker Compose
    - Understanding Docker Compose YAML
    - Structure of Docker Compose YAML
    - Automated/Orchestrated Provisioning of Docker Containers using Docker Compose
    - Manage Docker Containers using Docker Compose