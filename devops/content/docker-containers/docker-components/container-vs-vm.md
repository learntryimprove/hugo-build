+++
title = "Container vs VM"
weight = 8203
chapter = false
pre = "<b>2.3 </b>"
+++

![Docker Components Docker Containers](/images/docker-containers/docker-components-docker-containers.png)


---
Proceed with next page in this Book [Prerequisites for Docker](/docker-containers/docker-components/prerequisites-for-docker)