+++
title = "DevSecOps - Getting Hands-on"
weight = 11
chapter = false
+++
---

This Page is intended to get details aligned for better security issues identification and resolution in infrastructure

** Stages **
* [SAST](#sast)
* [Secret Detection](#secret-detection)
* [Code Quality](#code-quality)
* [DAST](#dast)
* [IAST](#iast)
* [API Security](#api-security)
* [Fuzz Testing](#fuzz-testing)
* [Dependency Scanning](#dependency-scanning)
* [License Compliance](#license-compliance)
* [Vulnerability Database](#vulnerability-database)
* [Security Benchmarking](#security-benchmarking)
* [Attack Emulation](#attack-emulation)
* [Vulnerability Management](#vulnerability-management)
* [Vulnerability Scanning](#vulnerability-scanning)
* [Penetration Testing](#penetration-testing)
* [Fixing reported observations](#fixing-reported-observations)
* [OS Patching](#os-patching) 
* [Tools](#tools)  
* [References](#references)
 
---
##### SAST
- Static Application Security Testing scans the application source code and binaries to spot potential vulnerabilities before deployment using open source tools 
- Tool Used: Sonarqube
- Priority: high 

---
##### Secret Detection
- Check for credentials and secrets in commits. This category is at the "viable" level of maturity.
- Tool Used: XYZ
- Priority: medium

---
##### Code Quality
- Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit. 
- Tool Used: XYZ
- Priority: medium

---
##### DAST
- Dynamic Application Security Testing analyzes your running web application for known runtime vulnerabilities. 
- It runs live attacks against a Review App, an externally deployed application, or an active API, created for every merge request 
- Tool Used: XYZ
- Priority: high 

---
##### IAST
- Interactive Application Security Testing checks runtime behavior of applications by instrumenting the code and checking for error conditions. 
- It is composed by an agent that lives inside the application environment, and an external component, like DAST, that can interact and trigger unintended results.
- Tool Used: XYZ
- Priority: low 

---
##### API Security
- API Security focuses on testing and protecting APIs. 
- Testing for known vulnerabilities with DAST API and unknown vulnerabilities with API Fuzzing, API Security runs against a live API or a Review App to discover vulnerabilities that can only be uncovered after the API has been deployed. 
- Users can provide credentials to test authenticated APIs. 
- Tool Used: XYZ
- Priority: high

---
##### Fuzz Testing
Fuzz testing increase chances to get results by using arbitrary payloads instead of well-known ones. This category is at the "viable" level of maturity.
Tool Used: XYZ
Priority: high

---
##### Dependency Scanning
Analyze external dependencies (e.g. libraries like Ruby gems) for known vulnerabilities on each code commit 
Tool Used: Dependency Track
Priority: high 

---
##### License Compliance
Upon code commit, project dependencies are searched for approved and blacklisted licenses defined by custom policies per project. 
Tool Used: LicenseFinder
Priority: medium

---
##### Vulnerability Database
In order to maintain the efficacy of those scans, we strive to keep their underlying vulnerability databases up-to-date.
Tool Used: XYZ
Priority: high

---
##### Security Benchmarking
Secure stage benchmarking for measuring security effectiveness in detecting security findings.
Tool Used: XYZ
Priority: ???

---
##### Attack Emulation
Continuously assess your applications and services are not vulnerable to security threats through automated, real-world emulated scenarios to identify weaknesses in your attack surface
Priority: low

---
##### Vulnerability Management
View, triage, trend, track, and resolve vulnerabilities detected in your applications. This category is at the "viable" level of maturity.
Tool Used: XYZ
Priority: high 

---
##### Vulnerability Scanning
Coming Soon... 

---
##### Penetration Testing
Coming Soon...

---
##### Fixing reported observations
Coming Soon...

---
##### OS Patching 
- Few OS Patching is expected to be carried out as quarterly/half yearly activity 
- Few OS Patching is expected to be carried out for each event of code promotion to higher environment. 

---
##### Tools  
- DEPENDENCYTRACK - [Link](https://docs.dependencytrack.org/) 
- ZAP - [ZAPping the OWASP Top 10 (2021)](https://www.zaproxy.org/docs/guides/zapping-the-top-10-2021/)

---
##### References
- OWASP Scanning Tools list - [Vulnerability Scanning Tools | OWASP Foundation](https://owasp.org/www-community/Vulnerability_Scanning_Tools) 
- OWASP Source code analysis Tools list  - [Source Code Analysis Tools | OWASP Foundation](https://owasp.org/www-community/Source_Code_Analysis_Tools)
- GITLAB Security - Product Stage Direction - [Secure](https://about.gitlab.com/direction/secure/#vulnerability-database)
- Dependecy Tracker - [Link](https://plugins.jenkins.io/dependency-track)
- Jenkins Integrations ZAP - [Link](https://we45.com/devops/a-step-by-step-guide-to-integrate-zap-with-jenkins)
- SnarQube - [Download | SonarQube](https://www.sonarqube.org/downloads/)