+++
title = "The right Metrics"
weight = 8
chapter = false
+++

---
- [What Should We Measure and Why](#what-should-we-measure-and-why)
- [Effectiveness](#effectiveness)
- [Efficiency](#efficiency)
- [Sustainability](#sustainability)
---

![Metric Relationship](/images/devops-devsecops/07-metrics-relationship.png "Metric Relationship icon")

### What Should We Measure and Why
- **Why Metrics are important?** - Metrics inform improvements.
- **What should we measure?** 
- **What are common problems faced?**
- **How do we avoid the pitfalls?**

![Metrics Inform Improvement](/images/devops-devsecops/07-metrics-informed-inprovement.png "Metrics Inform Improvement Icon")

![Cobra Effect](/images/devops-devsecops/07-cobra-effect.png "cobra effect icon")

![cynefin](/images/devops-devsecops/07-cynefin.png "cynefin icon")

---
### Effectiveness
- OKRs at multiple level can become metrics.
- Team OKR - Make Pipeline configuration easy and automated
    - Reduce support request by 50%
    - Reduce platform onboarding from 3 days to 10 mins
    - Increase number of on-boarded repositories from 5 to 20
- Organization OKR - Deliver irresistible developer experience 
    - Hold 2 user feedbacks per month
    - Improve developer rating from 3 to 4 stars
    - Increase platform users from 20 to 200
    - Publish 3 online training classes
---
### Efficiency
- Flow efficiency indicates how much time is spent waiting relative to the total time. 
- To prevent preserved incentives, every metric needs one or more metric that can identify that behavior change.
---
### Sustainability
- Measuring health indicators are as important as measuring efficiency, Team health is leading indicator for quality, stability and security. Burnout decreases quality and increases turnovers.
- Technical Debt is a business problem, As the complexity of every change increases, throughput and quality decreases.

---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home