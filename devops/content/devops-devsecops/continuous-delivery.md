+++
title = "Continuous Delivery"
weight = 6
chapter = false
+++

---
- [Why CD?](#why-cd)
- [Is It Just Tools?](#is-it-just-tools)
- [Common Challenges](#common-challenges)
- [Exploring the Pipeline and Getting Started](#exploring-the-pipeline-and-getting-started)
---
### Why CD?
- Continuous Delivery is the ability to get changes of all types into production, or into hands of users, safely and quickly in a sustainable way.
- Lets Understand **Legecy Delivery**
    - Receive the requirements
    - Build everything to spec
    - Send it to QA for testing 
    - Deliver Everything 
- Failure, which is not an option its a promise. Instead preparing for Failure is optional 
- Why do We fail ?
    - The requirements are wrong
    - OR misunderstood
    - OR change before we can deliver 
    - OR wont even work in production
- What CD Mitigates 
    - Removes uncertainty early
    - Reduces the cost of making change
    - Improves speed of quality feedback 
    - Minimizes wasted efforts 
---         
### Is It Just Tools?
- Continuous delivery improves both delivery performance and quality and also helps improve culture and reduce burnout and deployment pain
- However implementing these often requires rethinking everything
- Change of Mindset needed, 
    - Deliver First : Establish the ability to verify assumptions quickly and safely 
    - Ability to deliver is out highest priority. Keep the pipeline green!
    - Small and frequent changes, not big bang releases
    - Automate!
    - Rapid feedback to improve hypotheses, not assumptions of success
    - Remove Toil and work smaller not work faster.
    - Breakdown communication silos and minimize handoffs
    - Build quality in. Quality is a process, not a job
- Impact to Teams,
    - Improved Quality
    - Reduce process toil
    - Improved Teamwork
    - Improved Moral
    - Note: Teams that do not have birth to death ownership will struggle to deliver the quality outcomes. 
- Impact to Organization,
    - Improved communication
    - Improved collaboration
    - Improved value delivery
    - Happier Teams
    - Happier Customers 
    - Ability to try new ideas with less cost or risk.
- CD is much much more than just build and deploy. It represents an organization's circulatory system.
---
### Common Challenges
- Challenges at Teams level,
    - "We automated the Build, We are doing CD!", This mindset may not get expected output of CD
    - Testing Knowledge
    - Work Decomposition
    - Teamwork
    - Process overhead
- Challenges at Organizational level,
    - Functional Silos 
    - Lack of Good platform 
    - Misaligned Goals
    - Weakly defined products
---
### Exploring the Pipeline and Getting Started
- CD is not "Automate what we have", it needs to be planned well.

![Continuous Pain](/images/devops-devsecops/05-continuous-pain.png "Continuous Pain Icon")

- In Reality CD looks like below,

![Continuous Delivery](/images/devops-devsecops/05-continuous-delivery-reality.png "Continuous Delivery Icon")

---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home