+++
title = "DevOps & DevSecOps"
weight = 1005
chapter = false
+++

---
### 1. Welcome
- Welcome!
### 2. DevSecOps Background and Historical Perspective
- What Is DevSecOps and Why It Is Important?
### 3. Tech Like I'm Ten
- Tech Like I'm 10
### 4. Value Stream Management
- Value Stream Management
### 5. Platform as Product
- Platform as Product
### 6. Continuous Delivery (CD) Basics
- Continuous Delivery (CD): The Basics
### 7. The Power of Culture
- The Power of Culture
### 8. The Right Metrics
- Metrics That Matter
### 9. Agile Contracting
- Agile Contracting
### 10. Cybersecurity
- Cybersecurity
### 11. GEtting Hand On
---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home