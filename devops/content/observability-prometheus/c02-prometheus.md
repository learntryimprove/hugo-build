+++
title = "Installing Prometheus Server"
weight = 12002
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../c01-prometheus) 
---
There are multiple ways you get a Prometheus server installed. Broadly can be categorised into two purposes 
#### 1. For development usage

Lets understand how one can get Prometheus up and running for development usage,

1. By downloading the binaries for your OS and run the executable to start the application.
{{< tabs >}}
{{% tab name="Linux" %}}
```
wget https://github.com/prometheus/prometheus/releases/download/v2.26.0/prometheus-2.26.0.linux-amd64.tar.gz -p /tmp
sudo tar -xvf  /tmp/prometheus-2.26.0.linux-amd64.tar.gz -C /usr/local/share/
sudo mv /usr/local/share/prometheus-2.26.0.linux-amd64/ /usr/local/share/prometheus
/usr/local/share/prometheus
```
{{% /tab %}}
{{% tab name="windows" %}}
```
To Do 
```
{{% /tab %}}
{{< /tabs >}}
2. Using Vagrant [Click here for detailed instructions](/work-in-progress/)
3. Using Docker Container with the following command provided you have working docker workspace setup on your laptop 
  ``` 
  docker run --rm -it -p 9090:9090 prom/prometheus 
  ```

#### 2. For production usage

In order to use prometheus in production environment one needs to plan it well. There are multiple ways you can get Prometheus installed for production usage,
1. Installation on a desired OS
    - To Do 

2. Installaton on a dockerized platform
    - To Do 

---
##### [Next](../c03-prometheus)
