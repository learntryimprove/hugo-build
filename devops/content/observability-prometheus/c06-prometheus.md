+++
title = "Configuring New Target in Prometheus"
weight = 12006
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../c05-prometheus) 
---

  - configuring node exporter
  - configurnng prometheus
  - applying changes without prometheus restart using static discovery
  - applying changes using prometheus webhook
  - applying changes using prometheus restart


---
##### [Next](../c07-prometheus)
