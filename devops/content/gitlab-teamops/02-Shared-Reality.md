+++
title = "Shared Reality"
draft = false
weight = 2
+++

---
The first Guiding Principle of TeamOps: **Shared reality**
- [Shared reality](#shared-reality)
- [Public by default](#public-by-default)
- [Single Source of Truth (SSoT)](#single-source-of-truth-ssot)
- [Low-context communication](#low-context-communication)
- [Say why, not just what](#say-why-not-just-what)
- [Situational leadership](#situational-leadership)
- [Shared values](#shared-values)
- [Inclusivity](#inclusivity) 

---
## Shared reality 
- While other management philosophies prioritize the speed of **knowledge transfer**, TeamOps optimizes for the speed of **knowledge retrieval** in company-wide documentation.
- All teamwork must be based on and informed by an **objective — and shared — reality**. 
- A collective employee experience that your team shares to develop trust, direct contributions, and motivate productivity. 
- Typically, this shared reality is composed of three elements:
    - **Information** - All team members should be able to autonomously consume the information and resources that enable productivity in their role.
    - **Objectives** - Ironically, the day-to-day of your shared reality is usually defined by what you are trying to bring to reality – a result that you’re collaborating to develop.
    - **Values** - Employee experience is influenced by more than company operations, it’s most directly manifest as team behaviors which can be observed, recorded, and quantified.
- Once defined and activated, this reality then is preserved for continuous and universal access and accountability through documentation in a **knowledge management system, or “single source of truth.”**

---
## Public by default
- Public by default, here means public to all employees.
- Conventional management philosophies may rely on intentional information silos or a **"need-to-know-basis"** model. This approach restricts transparency, with the goal of reducing misinformation. 
- **TeamOps flips this**: information should be public by default, with the goal of allowing maximum contribution. "Public by default" requires an organization to designate which information is explicitly not public, creating a bias for transparency across all functions of a business.
- A traditional business problem is **"how do we get the right information to the right people at the right time?"** 
- A TeamOps organization **asks**: "How do we create a system that allows everyone to access information and make contributions, regardless of role or function?"
- In practice, a TeamOps organization uses a project management system that allows any team member to view data related to all other functions. A ***marketing manager***, for example, would be able to view a ***sales teams***' work and data, without requesting special access. 
- The system is free of walls and information silos. The result is that, instead of needing to create a system for unblocking access to information, management's role is to educate team members on how to use the information system, how to organize data, and how to self-serve.
- This type of system scales with much less effort and scalable leadership is effective leadership. 
- By writing guidance down transparently — in a way that others can modify, validate, or contribute to — leadership scales beyond an individual or team, and even beyond the organization.
- Here's an example: [Livestreaming company meetings on a branded YouTube channel](https://youtu.be/XcqloQezOUg)
- Shortly after GitLab Chief Revenue Officer Michael McBride joined the company in 2018, he [livestreamed a 1-to-1 meeting](https://youtu.be/XcqloQezOUg) with GitLab co-founder and CEO Sid Sijbrandij. As part of McBride's onboarding, Sid was asked to provide an impromptu pitch of GitLab.
- In a conventional organization, this interaction would likely be private and not recorded. By recording it and streaming it to the public on a branded YouTube channel, everyone is more informed — the two individuals on the call; GitLab team members past, present, and future; the wider community; customers and partners; candidates; etc.

---
## Single Source of Truth (SSoT) 
- To maximize universal information accessibility, TeamOps intentionally structures all information data (policies, objectives, workflows, instructions, values, etc.) in a virtual knowledge management system referred to as a [single source of truth (SSoT)](https://about.gitlab.com/handbook/values/#single-source-of-truth). 
- It is founded on the thesis that decisions are [better informed](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/) when there is no such thing as a "latest version." There is only the version.
- Teams and functions may choose different mediums as the **SSoT** depending on the task and the nature of their work. 
- TeamOps allows this type of SSoT flexibility, but requires that those who dictate the SSoT share that information transparently and [crosslink](https://about.gitlab.com/handbook/communication/#cross-link) where appropriate.
- This type of system scales and maintains workforce awareness and empowerment with much less effort than word-of-mouth updates or memos. 
- By writing guidance down transparently — in a way that others can modify, validate, or contribute to — leadership scales beyond an individual or team, and even beyond the organization. 
- GitLab refers to this approach as [handbook-first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). A core outcome is that individuals are less dependent on others for the information they need to do their job.
- Here's an example: [Maintaining a single list of all applications used within a company](https://about.gitlab.com/handbook/business-technology/tech-stack/)
- Maintaining a single source of truth for elements that few individuals need isn't difficult (e.g. using a single, persistent shared document for a [1-to-1 meeting](https://about.gitlab.com/handbook/leadership/1-1/) between a people manager and direct report). 
- Honoring the single source of truth is harder for elements which are cross-functional by design, such as a list of all applications used across the entire organization.
- In conventional organizations, multiple departments may maintain their own version of this list. Crucially, this is either not acknowledged as a problem, or there is no known solution to removing duplication. 
- In an organization powered by TeamOps, duplicate sources may exist initially. Once duplication is discovered, everyone can contribute to the solve (collaborating to create a SSoT) and one's default is to actively work to remove duplication. 
- At GitLab, the application list [lives in our handbook](https://about.gitlab.com/handbook/business-technology/tech-stack/), and integrity is ensured thanks to [version control](https://about.gitlab.com/topics/version-control/).

---
## Low-context communication
- [Low-context communication](https://about.gitlab.com/company/culture/all-remote/effective-communication/) assumes that the person you're communicating with has little or no context about the topic at hand. 
- This means the person delivering the information is responsible for making sure the communication includes everything the recipient will need to understand the message and make an informed response, such as SSoT links, definitions, relevant team members, or updates. 
- This empowers individuals to make decisions and take action without needing to ask unnecessary follow-up questions that could have been avoided.
- Assuming that recipients of your communication do know know anything about the topic at hand and wish to learn as much as possible in as little time as possible, all low-context communication should be:
    - Explicit, not implicit
    - Direct, not indirect
    - Simple, not complex
    - Comprehensive, not narrow.
- Each business function may have unique expectations on low-context communication (e.g. what classifies as low-context in sales may not in engineering). If decisions within a function appear to be ill-informed, audit the expectations on context first.
- Here's an example: [Making a company-wide announcement that meaningfully changes a policy](https://about.gitlab.com/handbook/communication/#how-to-make-a-company-wide-announcement)
- At GitLab, a department leader will typically send out a [company-wide message](https://about.gitlab.com/handbook/communication/#how-to-make-a-company-wide-announcement) to a Slack channel that includes every team member. Crucially, this message does not include only the news, but a link to a GitLab merge request detailing what changed ([diffs](https://docs.gitlab.com/ee/development/diffs.html)).
- The [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104440) which added the very copy you're reading now is an example of low-context communication in practice. Darren M., the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) for the change, also shares a link to the handbook and/or project page ("the news"). The merge request includes context on what's changing, and details on where to ask questions and contribute new iterations (including an optional [Ask Me Anything (AMA)](https://about.gitlab.com/handbook/communication/ask-me-anything/) session). This gives any team member enough context to share feedback and apply these changes to their own teams in an informed way.

---
## Say why, not just what
- It can be **tempting** for a risk-intolerant organization to announce a change without much context in hopes that it will "fly under the radar" to avoid controversy and force the weight of answering questions onto mid-level managers. 
- TeamOps organizations recognize that up-front transparency is a foundational element to employee autonomy, transparent documentation and business continuity. This requires announcements, updates, and decisions to be shared not only with what the change is, but also [why](https://about.gitlab.com/handbook/values/#say-why-not-just-what) it's being made.
- While saying "why" does not mean justifying every decision against other alternatives, but it does require a leader to [articulate their reasoning](https://about.gitlab.com/handbook/values/#articulate-when-you-change-your-mind). This prevents speculation and [builds trust](https://about.gitlab.com/handbook/leadership/building-trust/), which is one of the traits of being a [great remote manager](https://about.gitlab.com/company/culture/all-remote/being-a-great-remote-manager/).
- Beyond these cultural benefits, saying "why" also supports knowledge management by creating institutional memory that is powerful for the future efficiency of your organization. 
- When a related change is being made in the future, the new decision maker (or Directly Responsible Individual) will have access to the [well-documented](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/#why-does-handbook-first-documentation-matter) reasoning behind past decisions. They're able to better understand the context, avoid duplicating work or mistakes, and make more consecutive decisions.
- Here's an example: [Updating GitLab's recruitment privacy policy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107652)
- GitLab's Recruitment Privacy Policy was updated. Rather than updating the policy behind closed doors, the [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107652) outlines the why. 
- It provides context into how the change enables cross-functional groups to work more efficiently. The explanation of why enables more thoughtful conversation around a potentially polarizing topic (privacy).

---
## Situational leadership
- Informed decisions require the use of a key phrase: "It depends." 
- In a TeamOps organization, individuals and people managers alike understand that different scenarios are best managed with different strategies. So, each leader is responsible for assessing the unique variables (such as urgency, emotions, group dynamics, or project metrics), then seeking whatever information is needed to uniquely and appropriately respond to the situation at hand. This may require a variety (or even a combination) of leadership strategies and tactics.
- Adopting a [situational leadership strategy](https://about.gitlab.com/blog/2021/11/19/situational-leadership-strategy/) brings an added layer of intelligence to the way leaders manage each individual, project, and decision. 
- TeamOps requires leaders to adapt the way they communicate, provide guidance, and delegate work based on a list of weighted factors and considerations. This strategy enables more informed decisions, prevents a reliance on status quo, and presents new growth opportunities for team members.
- [Situational leadership](https://situational.com/situational-leadership/) **does not only apply to people managers**. It also applies to individual contributors, since everyone is expected to be a [manager of one](https://about.gitlab.com/handbook/leadership/#managers-of-one).
- Here's an example: [When to let others lead, and when to lead directly](https://youtu.be/Q5HPHZKecrQ?t=854)
- [Situational leadership](https://about.gitlab.com/blog/2021/11/19/situational-leadership-strategy/) is exemplified when a leader adjusts behavior from scenario to scenario, rather than carrying emotions or biases from one scenario directly into the one they encounter next. 
- In [this recorded meeting](https://youtu.be/Q5HPHZKecrQ?t=854), GitLab's CEO Sid shares two examples: 
    - In the first, he handed off a proposal and gave the team freedom to improve it. 
    - In the second, he pivoted to risk-reduction mode and required that every minor update go through his personal approval.
- In both cases, the leadership fits the scenario. In both cases, Sid began as "**it depends**," and adjusted his approach as the variables were revealed.
- A **third example** is the meeting taking place in this video itself: working on an early iteration of this very content. The stakes are lower, the audience is smaller, and revisions are [two-way doors](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions) (easily reversible). By seeking information on the audience, timeline, and impact, Sid is able to delegate more, reduce approval loops, and lean away from urgency. After all, "It depends."!

---
## Shared values
- The reality of your teams’ day-to-day life at work – or employee experience – is influenced by more than company operations, it’s most directly manifest as team behaviors which can be observed, recorded, and quantified. 
- These behaviors are best designed and managed in the context of a shared set of organizational values, which are one of the [prerequisites](https://about.gitlab.com/handbook/teamops/#prerequisites-for-TeamOps) for TeamOps. Without explicit cultural values, there is no group identity or basis for group cohesiveness.
- Each organization may create its unique values, but whatever they are, values must be recorded in the SSoT for reference and accountability, then operationalized through habits, rituals, and workflow practices. Include definitions, examples, and measurable success criteria so that any team member within the organization can understand and activate them.
- The guard rails created by explicit, shared values provide more freedom for individual decision making. This leads to more informed decisions by removing guesswork on whether (or how) values were applied during operational processes.
- Here's an example: [20+ ways GitLab values are integrated into decision making](https://about.gitlab.com/handbook/values/#how-do-we-reinforce-our-values)
- GitLab [reinforces its values in over 20 different ways](https://about.gitlab.com/handbook/values/#how-do-we-reinforce-our-values), including what we select for during hiring, our default software settings, criteria for discretionary bonuses and promotions, and what we explicitly call out when making decisions. These intentional integrations increase the likelihood that decisions are informed by shared values.

---
## Inclusivity 
- Decisions and results are better informed when they include a maximally [diverse array of perspectives](https://about.gitlab.com/handbook/values/#seek-diverse-perspectives). While team members should always be empowered to work autonomously, they should still remain included as a collaborator, informed as a colleague, and valued as a critical component of success.
- In TeamOps, a [bias for asynchronous communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication) fosters [inclusion](https://about.gitlab.com/company/culture/inclusion/) of many diverse personas, including underrepresented groups, cultural / geographic populations, and neurodiverse. By defaulting to written, asynchronous sharing, everyone contributes in the same size font, which standardizes and equalizes the weight of each written message. People of all backgrounds, abilities, and work styles are invited to participate in a way that serves their needs. The best idea wins, not the loudest voice in the meeting.
- TeamOps frees contributors from the conventional bounds of time zones and meetings, and invites a wider audience to participate in a shared reality. This generates more informed contributions from more parties, more thoughtful conversation, and more archived context for retrospectives and evaluations.
- Here's an example: [Changing a company operating principle](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/103753)
- Samantha L., a leader in GitLab's learning & development team hosted [Crucial Conversation](https://about.gitlab.com/handbook/leadership/crucial-conversations/) cohorts for six months. She noticed a common theme: people consistently struggled to say "no" at work. Rather than hosting a closed-door meeting to change an operating principle to address this, Samantha proposed a change in a public forum ([a GitLab merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/103753)).
- Additionally, she posted a Slack message in the public `#values` channel asking for feedback and suggestions from anyone who felt compelled to contribute. Ultimately, [the DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) of the impacted handbook page — [GitLab Values](https://about.gitlab.com/handbook/values/) — came to a decision that was more informed, as it included a more diverse range of perspectives. 
- The feedback is also well-documented for future reference and iterations.

---
Reference : [Gitlab TeamOps](https://levelup.gitlab.com/learn/course/teamops)