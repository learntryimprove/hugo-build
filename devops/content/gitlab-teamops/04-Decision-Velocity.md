+++
title = "Decision Velocity"
draft = false
weight = 4
+++

---
The third Guiding Principle of TeamOps: **Decision velocity**
- [Decision velocity](#decision-velocity)
- [Collaboration codification](#collaboration-codification)
- [Push decisions to the lowest possible level](#push-decisions-to-the-lowest-possible-level)
- [Bias for action](#bias-for-action)
- [Boring solutions](#boring-solutions)
- [Stable counterparts](#stable-counterparts)
- [Collaboration is not consensus](#collaboration-is-not-consensus)
- [Only healthy constraints](#only-healthy-constraints)

---
## Decision velocity
- Achieving faster, better results depends on decision-making velocity – a team’s ability to increase the quality and quantity of decisions made in a particular stretch of time through behavioral and logistical agreements.
- Decisions are the fuel for high-performance teams, and represent a key success metric in the future of work. The more decisions are made, the more results can come from them. Conventional management philosophies often strive for consensus to avoid risk instead of developing a bias for action, which can result in delays, confusion, or unnecessary conflict.
- In TeamOps, success is correlated with the group’s decision velocity, which is evidenced by the average duration of a collaboration process, and quality, value, or accuracy of the changes made from a decision.
---
## Collaboration codification
- Most organizations are so focused on finalizing a decision, that they neglect the critical precursor to a successful agreement: ***setting the standards about how that decision will be made***. 
- Building an intentionally designed workstream for decisions and projects to flow through can help align expectations, reinforce the shared reality, and minimize unexpected barriers to success.
- Collaboration codification is the articulation and documentation of cultural practices, software standards, and behavioral expectations that helps standardize employee experience within an organization. 
- This includes how company values are visible in workstreams, communication channels, rituals, and tool usage. The ability to collaborate more effectively and universally will ensure that miscommunication isn’t unnecessarily stalling a decision or result.
- Codification information should be included in your team's **Single Source of Truth** for easy reference and continuous accountability. Depending on the format of your knowledge management system and the volume of codification you have, it could either be a dedicated section, or woven throughout all of the pages.
- Here's an example: [Setting Internal Communication Guidelines for Standardized Tool Use](https://about.gitlab.com/handbook/communication/)
- To minimize miscommunications that can stem from cultural diversity, contextual interpretations, or various levels of software experience, GitLab maintains a handbook page about [internal communication guidelines](https://about.gitlab.com/handbook/communication/). 
- These rules, instructions, and demonstrations ensure that our internationally distributed workforce is using the same tools in the same way, and handing off results to one another without the risk of important information getting **“lost in translation.”**

---
## Push decisions to the lowest possible level
- As many decisions as possible should be made by the person doing the work (the DRI), not by their manager or their manager's manager. 
- This ownership not only supports agency by empowering each person to directly and immediately make necessary changes to their work, but also increases efficiency by eliminating delays while waiting for approval, and frees senior leaders from the burden of making decisions that stunt their own productivity.
- In the spirit of iteration, most times, it's better to execute quickly on a sub-optimal decision with full conviction (then return later to improve upon the decision based on post-decision feedback), rather than executing on a full decision with sub-optimal conviction. 
- The DRI of each project knows the moving parts and impacts of a choice more than anyone else, and should be trusted with full accountability over it.
- Here's an example: [Updating Developer Evangelism mentoring guidelines](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107903)
- A Senior Developer Evangelist at GitLab recognized that many coaching and mentoring sessions are shared in private 1:1 conversations. In an effort to add context and transparency to the process — thereby enabling other developer evangelists to make more decisions on their own — he [documented and merged feedback examples](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107903). 
- The person doing the work is empowered to make the decision. In this example, the decision involved many micro decisions: to document or not; what context to add; where to document; what examples to share; how to share within the company.

---
## Bias for action
- Ideation, collaboration, and execution are all accelerated when a team maintains a [bias for action](https://about.gitlab.com/handbook/values/#bias-for-action) as opposed to alternatives like alignment and consensus. 
- This bias stems from the agency and ownership that each individual is empowered with, and can then use that autonomy to optimize their own proactivity, self-efficacy, and creativity. A conventional employee might have a mindset of, **“Should I?”** But a TeamOps participant can instead think, **“I will.”**
- When facing decisions that may involve imperfect information or failures, having a bias for action ensures a more rapid pace of execution. 
- As a team, this may require a tolerance for mistakes and an appreciation for [two-way door](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions) decisions, which should be discussed as a shared reality and collaboration code.
- Here's an example: [Coursera Remote Work course development](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1128)
- In September 2020, much of the knowledge-working world began to seek training on how to manage remote teams. As the COVID-19 pandemic forced many teams to work primarily from home, GitLab was well-positioned to provide proven practices for others to learn from and contribute back to. 
- [Jessica R](https://gitlab.com/jessicareeder).'s bias for action led to a question: "***What if we partner with a leader in online learning to teach the world a skill that it needs right now?***" This GitLab epic is a case study in execution, sparked by a bias for action. Every iteration is documented. The output is [How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management), a free-to-take course on Coursera with 40,000 learners (and growing!).

---
## Boring solutions
- It is tempting to seek out the most cutting-edge, complex, or interesting solution to solve a problem. Instead, TeamOps encourages choosing ["boring" or simple solutions](https://about.gitlab.com/handbook/values/#boring-solutions). 
- By taking every opportunity to reduce complexity in the organization, you're able to increase the speed and frequency of innovation.
- One boring solution you may often see: researching what other successful organizations are doing and adopting their methods, rather than reinventing a process.
- Embracing boring solutions and shipping the [minimum viable change (MVC)](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimal-viable-change) also means [accepting mistakes](https://about.gitlab.com/handbook/values/#accept-mistakes if that solution doesn't work, and moving on to the next [iteration](https://about.gitlab.com/handbook/values/#iteration). Because changes are small, mistakes are far less costly. 
- This encourages more decision-making in a shorter span of time.
- Here's an example: [Solving a GitLab attribution problem by improving git commit message](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues/130)
- Nick V., a director at GitLab, [proposed a boring solution](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues/130) to use existing functionality in new ways. By modifying a few lines in an automated message, he was able to solve an organization-wide problem with attribution.
- In a TeamOps organization, boring solutions are celebrated because of their simplicity. There is always a possibility to add more polish or functionality, if it's needed in the future. The initial boring solution enables more decisions to be made, more quickly.

---
## Stable counterparts
- Often, decision velocity is easily maintained inside of team, but then slows when information is shared outside of it. 
- It is essential that various contributors throughout the organization can be looped into a decision for review, collaboration, and feedback with minimal impact.
- In a [stable counterparts model](https://about.gitlab.com/blog/2018/10/16/an-ode-to-stable-counterparts/) for enabling cross-functional execution, every functional team (e.g. Support) works with the same team members from a different functional team (e.g. Development). 
- As a member of one function, you always know who your partner in another function will be. **Stable counterparts** are an intentionally chosen structure designed to execute on decisions. 
- They foster collaboration across functions by giving people stable counterparts for other functions they need to work with to execute decisions. This enables more social trust and familiarity, which [speeds up decision making](https://about.gitlab.com/blog/2018/10/16/an-ode-to-stable-counterparts/), facilitates [stronger communication flows](https://about.gitlab.com/handbook/teamops/informed-decisions/), and reduces the risk of conflicts. 
- Stable counterparts enhance cross-functional execution without the downsides of a [matrix organization](https://about.gitlab.com/handbook/leadership/#no-matrix-organization).
- Relevant to this is the importance of singular management. Like a project or decision should only have one Directly Responsible Individual (DRI), an individual should only have one manager. 
- Conventional management philosophies may focus on minimizing the shortcomings of matrix organizations (or, "dotted line" reporting structures) but refuse to eliminate them. TeamOps asserts that a [no-matrix organization](https://about.gitlab.com/handbook/leadership/#no-matrix-organization) is not only feasible, but essential to executing decisions. By ensuring that each individual reports to exactly one other individual, feedback and approval processes are as streamlined as possible.
- Here's an example: [Technical Support stable counterparts](https://about.gitlab.com/handbook/support/support-stable-counterparts.html)
- Support team members are [assigned a permanent contact](https://about.gitlab.com/handbook/support/support-stable-counterparts.html) for a GitLab team member within another function in the company. The ability to build long-term relationships is the foundational benefit of having stable counterparts. 
- Repeated interactions help us understand personal workflows and communication styles, so we know how to most effectively execute decisions with our counterparts.

---
## Collaboration is not consensus
- TeamOps unlocks your organization's potential for making many decisions, by challenging the idea that consensus is productive. 
- Organizations should strive to have smaller teams iterating quickly but transparently (allowing everyone to contribute), rather than a large team producing things slowly as they work toward consensus.
- Leaders and managers must moderate the desire to be involved in every decision. 
- [Permissionless innovation](https://about.gitlab.com/handbook/values/#collaboration-is-not-consensus) increases a team's [bias for action](https://about.gitlab.com/handbook/values/#bias-for-action) and the number of decisions being made. 
- If you choose the right [directly responsible individual (DRI)](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) and empower them to work transparently, you should not expect them to wait for a brainstorming meeting or group sign-off.
- Feedback should be documented transparently. The DRI must review all feedback, but they are not required to respond to everything. This can be challenging for teams and managers, when contributions and ideas don't receive a reply. However, the experience is far superior to decisions being made in private, with limited visibility and fewer opportunities for healthy discussion.
- Here's an example: [Implementing a replacement program for GitLab Contribute in FY23](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700)
- GitLab cancelled its FY23 Contribute event due to COVID risk posed to team members from a large, global event. Many decisions were necessary in order to implement a replacement initiative. This principle enabled the DRI (Directly Responsible Individual) to ingest a lot of thoughtful feedback in a [Manager Mention Merge Request for a FY23-Q3 Visiting Grant Program](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700). 
- Although not every comment was replied to, everyone at the company was able to contribute feedback. Ultimately, the feedback was addressed and decisions were made in a 25-minute sync session, enabling GitLab team members to start planning their FY23-Q3 travel plans.

---
## Only healthy constraints
- Organizational growth does not have to result in stagnation. Leaders and managers must resist the tendency to put new processes and approvals are put in place because it's how other "mature" companies operate, slowing the pace of decision-making and innovation (e.g. [Slime Molds](https://komoroske.com/slime-mold/)).
- TeamOps requires an awareness of the constraints that develop as a company grows, and an active approach toward removing them. 
- TeamOps leaders must know the difference between [healthy (useful) constraints and unhealthy ones](https://about.gitlab.com/handbook/only-healthy-constraints/), and create a system for unblocking and reducing inefficiencies in the company. Ideally, a TeamOps organization can continue to operate with the agility of a startup while realizing the efficiencies of a scaling company.
- Like giving agency to an individual, healthy constraints help an organization analyze the appropriate levels of approval, audit if blocks are surfacing unnecessarily, and intentionally design boundaries that will protect team growth instead of limiting it.
- Here's an example: [GitLab's list of tactics to resist unhealthy constraints](https://about.gitlab.com/handbook/only-healthy-constraints/#resisting-unhealthy-constraints)
- In GitLab's S-1 filing, CEO Sid Sijbrandij documented some of the ways that GitLab plans to **"remain a startup"** and avoid the stagnation experienced by most early stage companies as they mature. 
- These tactics are also [documented in the company handbook](https://about.gitlab.com/handbook/only-healthy-constraints/#resisting-unhealthy-constraints) as a living, iterating list of ideas and recommendations that anyone in the company can use to resist unhealthy constraints.

---
Reference : [Gitlab TeamOps](https://levelup.gitlab.com/learn/course/teamops)