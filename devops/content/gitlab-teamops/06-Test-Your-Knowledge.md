+++
title = "Test Your Knowledge!"
draft = false
weight = 6
+++

---
## Foundation
- **What are the four Principles of TeamOps?"**
```
The four principles of TeamOps are as below,
- Teams exist to deliver results
- Teams must be informed by an objective, shared reality
- Everyone must be able to contribute
- Decisions are the fuel for high-performance teams
```
- **TeamOps decision making: is it centralized or decentralized?"**
```
TeamOps decision making is Decentralized. Individuals are empowered to make decisions without needing centralized approval.
```
- **True or false: You should iterate on and make changes to TeamOps for your own organization's benefit."**
```
True. TeamOps represents an ideal state, and it should be iterated on and evolved by everyone.
```
- **What are the prerequisites for TeamOps?"**
```
below are prerequisites of TeamOps,
- Communication guidelines
- Shared set of values
- Team trust
- Focus on results
- Culture of belonging
```
- **Is TeamOps for everyone in an organization?"**
```
Yes, TeamOps is for everyone in an organization. TeamOps is not exclusively for managers, leaders, or individual contributors. It is meant to be used by every member of an organization.
```
- **What does it mean that TeamOps represents an ideal state?"**
```
An ideal state is theoretical, and rarely exists. Instead of trying to apply TeamOps perfectly, allow the principles to guide your decisions.
```
---
## Shared Reality
- **With TeamOps, which roles are empowered to execute on decisions?"**
```
There are no limitations. Everyone can contribute, no matter your seniority or tenure.
```

- **What is a stable counterpart?"**
```
A partner in another function whom you work with consistently. This enhances collaboration, builds trust, and reduces risk of conflict between functions.
```

- **Who should be able to see an individual or team's KPIs?"**
```
Everyone. It is crucial that KPIs are documented and shared transparently across the organization.
```

- **Why is iteration a form of execution?"**
```
With TeamOps, executing on decisions is reframed as a series of iterations: smaller steps that break execution down into manageable pieces.
```

- **If you can't complete an objective by the due date, should you move the due date?"**
```
No. Instead of changing due dates, cut the scope. This allows you to maintain momentum and make progress.
```

- **What are some of the benefits of setting a due date for a project?"**
```
- Setting a due date...
- Builds trust
- Allows everyone to contribute to future progress
- Enables teams to execute on decisions
```


---
## Everyone Contributes
- **True or false: Information should be shared on a "need to know" basis."**
```
False. When using TeamOps, information should be public by default.
```

- **Where should information be stored?"**
```
In a Single Source of Truth (SSoT).
```

- **What are some of the benefits of low-context communication?"**
```
Low-context communication...
- Enables better informed decisions
- Doesn't require team members to have preexisting relationships or knowledge
- Documents the reasoning behind decisions
```

- **Why would you say "it depends" when evaluating a response?"**
```
Situational leadership means that the response may change depending on the context.
```

- **True or false: A diverse array of perspectives supports informed decision-making."**
```
True. When everyone can contribute in an inclusive way, the best idea wins.
```

- **What does it mean to say that values must be operationalized?"**
```
Company values should be more than just ideas. Team members within the organization must be able to understand and utilize them.
```


---
## Decision Velocity

- **What are boring solutions?"**
```
Simple solutions. With TeamOps, you should embrace the Minimum Viable Change (MVC) rather than proposing the most cutting-edge, complex, or interesting solution.
```

- **Why is collaboration different from consensus?"**
```
Collaboration allows everyone to contribute, while consensus requires everyone (or most people) to agree. Working toward consensus can slow progress.
```

- **Why should you 'say why, not just what'?"**
```
Transparently articulating the reasoning behind your decision builds trust, prevents speculation, and creates institutional memory.
```

- **What are some of the benefits of working asynchronously and focusing on documentation?"**
```
- Asynchronous workflows...
- Give individuals agency
- Create or reinforce a bias for action
- Bridge gaps in knowledge
```

- **True or false: TeamOps enables an organization to continue to operate with the agility of a startup while scaling."**
```
True. Resist the assumption that growth results in slower processes and less frequent decision-making.
```

- **What does it mean to push decisions to the lowest possible level?"**
```
Decisions should be made by the person doing the work or the DRI - not their superiors.
```

---
## Measurement clarity

- **True or false: TeamOps optimizes for knowledge retrieval."**
```
True. This is different from many management philosophies that focus on knowledge transfer.
```

- **Why is it important to have a low level of shame?"**
```
Iteration requires doing the smallest thing possible and getting it out as quickly as possible, which can create discomfort, but which results in fast decision making.
```

- **What does 'short toes' mean?"**
```
When people are empowered to make contributions outside of their direct domain — without worrying about "stepping on each others' toes" — decisions can be made faster.
```

- **What are some benefits to treating everything as if it's 'in draft'?"**
```
The freedom to make proposals...
- Removes bureaucracy
- Empowers smaller, faster changes
- Helps avoid playing politics
```

- **What's a 'two-way door' decision?"**
```
A decision that can be made without approval or consensus, and that is easy to reverse.
```

- **True or false: informal connections build trust."**
```
True. Leaders should encourage team members to prioritize social calls, chat channels, and other informal communication.
```