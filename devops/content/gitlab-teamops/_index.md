+++
title = "Gitlab & TeamOps"
draft = false
weight = 4004
+++

---
- [Introduction](#introduction)
- [TeamOps Foundation](#teamops-foundations)
- [Vision](#vision)
- [Principles](#principles)
- [Understanding And Applying TeamOps](#understanding-and-applying-teamops)
- [TeamOps Prerequisites](#teamops-prerequisites)
- [Test Your Knowledge!](#test-your-knowledge)
---
## Introduction 
- TeamOps is a new people practice that helps teams work better and faster, Introduced by Gitlab
- Every organization needs teams to deliver results, and the way teams intersect and interact has changed significantly.
- Tomorrow’s successful organizations need to move quickly and cohesively to achieve results.
- There are a lot of best practices already out there. At GitLab, Gitlab practice TeamOps, which is a more comprehensive approach.

![Common Pain Points](/images/gitlab-teamops/common-pain-points.png "Common Pain Points")

- It's a field-tested discipline that improves decision making in all work environments — whether you work remote, hybrid, or in-office.
- It focuses on the behaviors that make teams better and decisions faster.
- It's how GitLab scaled from a startup to a global public company in a decade. Now Gitlab sharing our strategy and our approach with every organization.
- The TeamOps Certification provides you with the key methods for evolving how your team collaborates and makes decisions.
- It’s fully self-serve and takes about 2 hours to complete, allowing you to learn at a time and pace that works best for your schedule.
- Let’s build a better and more inclusive future of work. I hope you enjoy this course – and take action by applying these principles to your teams.

---
## TeamOps Foundations
- TeamOps is a new people practice that brings precision and operations to how people work together. It's rooted in reality and objectivity, and focuses on the behaviors that make for better teams. It's supported by actionable tenets and concrete, real world examples.
- At its heart is a belief that creating the environment for better decisions and improved execution of them makes for better teams — and ultimately, progress.
- TeamOps is how GitLab scaled from a startup to a global public company in a decade. Now Gitlab opening it up to every organization.

TeamOps gives, 
- **A New Objective**, Results-Focused Management Discipline. TeamOps helps organizations make greater progress, by treating how your team members relate as a problem that can be operationalized.
- **A Field-Tested System**. Gitlab’ve been building and using TeamOps at GitLab for the past 5 years. As a result, our organization is more productive and our team members evidence greater job satisfaction. It was created here, but Gitlab believe it can help almost any organization.
- **Guiding Principles**. TeamOps is grounded in four Guiding Principles that can help organizations cooly, rationally navigate the dynamic, changing nature of work.
Action Tenets. Each Principle is supported by a set of Action Tenets – behavior-based ways of working that can be implemented immediately.
- **Real-World Examples**. Gitlab bring the Action Tenets to life with a growing library of real, output based examples of each Tenet as practiced at Gitlab.

---
## Vision
- It is GitLab's mission to make it so that everyone can contribute. When applied to management, this creates an atmosphere where everyone is empowered to lead.
- TeamOps differentiates itself from other management philosophies and people practices by consciously enabling decentralized decision making at a centralized (organizational) level.
- While guiding principles exist, TeamOps is not static. It is designed to be iterated on and evolved by everyone. This system is designed to apply to all work environments, whether you’re remote-first, office-first, or hybrid.
- By implementing TeamOps at an organizational level, individuals within the organization are less constrained. Each team member receives greater agency to exert self-leadership. Collectively, we believe this atmosphere allows for more informed decisions, made quicker, more frequently, and with a higher likelihood of successful execution.
- TeamOps is a recipe which has worked at GitLab. It may not be perfectly applicable in your company, and that's OK. As with our Remote Playbook, we are transparently sharing it to inspire other organizations and to invite conversation.

---
## Principles
- TeamOps is grounded in four Guiding Principles that can help organizations cooly, rationally navigate the dynamic, changing nature of work. Later in the course, you'll explore each principle in depth.

**The Four Guiding Principles**
1. Teams exist to deliver results.
- This is about achieving objectives. TeamOps’ decision-making principles are only useful if you execute and deliver results.
- A result is not a one-time event; rather, delivering a result establishes a new baseline. This allows future iterations to begin. In this way, focusing on execution empowers everyone to contribute to meaningful business outcomes.

2. Teams must be informed by an objective, shared reality.
- To make informed decisions, teams must have access to shared knowledge. While other management philosophies prioritize the speed of knowledge transfer, TeamOps optimizes for the speed of knowledge retrieval.
- Everyone can consume (self-serve) the same information, clearly documented in a single source of truth. Values manifest as behaviors, which can be observed and quantified. And wherever possible, teams must default to transparency.

3. Everyone must be able to contribute.
- Organizations must create a system where everyone can consume information and contribute, regardless of level, function, or location.
- When people don't have the opportunity to contribute because of their background, or where they live, or their life stage, we miss out on valuable perspectives.

4. Decisions are the fuel for high-performance teams.
- Conventional management philosophies often strive for consensus and avoid risk instead of developing a bias for action. This can result in slow decision-making.
- In TeamOps, success is correlated with decision velocity: the quantity of decisions made in a particular stretch of time (e.g. month, quarter) and the results that stem from faster progress.

---
## Understanding and Applying TeamOps
- TeamOps describes an ideal state. 
- In management, it is not possible to remain in an ideal state in perpetuity. Competing priorities, conflict tradeoffs, and coordination headwinds will be present at varying times.
- When applying TeamOps, resist the urge to take a binary approach. Rather than asking, "Have we completely achieved TeamOps in our team or company?," leverage TeamOps principles to navigate with more information and greater velocity.
- This is for individual contributors and people managers.
- The goal is to empower individual contributors to be better stewards of their own time and attention. Concurrently, TeamOps empowers people managers to lead with deeper conviction while creating more space for their direct reports to grow, develop, and contribute.

---
## TeamOps Prerequisites
- There are a number of foundational elements that should be in place in order for TeamOps to be maximally successful within a team or organization. These prerequisites consist of the processes, organizational structure, and culture that create an ideal environment to implement TeamOps principles.
- If your organization is missing some of these building blocks, consider this an opportunity to invest in your team. [GitLab's Remote Playbook](https://learn.gitlab.com/allremote/remote-playbook) can serve as a blueprint.

**What you'll need**
1. **Communication guidelines** - 
You'll want to have a [robust list of guardrails](https://about.gitlab.com/handbook/communication/) and tips outlining all aspects of communication within the organization. This includes how to approach sensitive topics, what tools to use for various types of interactions, and how to [embrace asynchronous communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/). There should be [no unwritten rules](https://about.gitlab.com/company/culture/all-remote/asynchronous/).
2. **Shared set of values** - 
Your [core values](https://about.gitlab.com/handbook/values/) must be more than words on a page. They should be actionable, clearly documented, and [reinforced in](https://about.gitlab.com/company/culture/all-remote/building-culture/#reinforcing-your-values) everything you do as a team. These values also act as a [filter for hiring](https://about.gitlab.com/company/culture/all-remote/building-culture/#how-do-i-assess-culture-fit-remotely), ensuring you continue to grow the team with people who are committed to living out these values in their work.
3. **Team trust** - 
Implementing new management techniques can be uncomfortable at first. A baseline of [trust](https://about.gitlab.com/handbook/leadership/building-trust/) across the organization will better enable the team to embrace change and assume positive intent along the way.
4. **Focus on results** - 
[Measuring output](https://about.gitlab.com/company/culture/all-remote/management/#focusing-on-results) instead of input is foundational to managing a distributed team. This means establishing clear, transparent goals so that team members at all levels of the organization can see and take ownership for how their work is contributing to the team, department, and company's success.
5. **Culture of belonging** - 
Prioritize cultivating an [inclusive environment](https://about.gitlab.com/company/culture/inclusion/) where team members feel a sense of belonging and [psychological safety](https://about.gitlab.com/handbook/leadership/emotional-intelligence/psychological-safety/). This unlocks the potential of your team and creates a [non-judgmental culture](https://about.gitlab.com/company/culture/all-remote/mental-health/#create-a-non-judgemental-culture) that welcomes diverse contributions and ideas.

---
Reference : [Gitlab TeamOps](https://levelup.gitlab.com/learn/course/teamops)