+++
title = "Everyone Contributes"
draft = false
weight = 3
+++

---
The second Guiding Principle of TeamOps: **Everyone contributes**
- [Everyone contributes](#everyone-contributes)
- [Asynchronous workflows](#asynchronous-workflows)
- [Directly responsible individual (DRI)](#directly-responsible-individual-dri)
- [Give agency](#give-agency)
- [Key review meetings](#key-review-meetings)
- [Short toes](#short-toes)
- [Disagree, commit, and disagree](#disagree-commit-and-disagree)
- [Informal communication to build trust](#informal-communication-to-build-trust)

---
## Everyone contributes
- Instead of relying on hierarchical management, organizations must create systems and channels where everyone can equally consume and contribute information, regardless of level, function, or location.
- If people don't have the opportunity to contribute because of their background, where they live, or their life stage, we not only miss out on valuable perspectives, but also the creative solutions and essential productivity that each team member can provide.
- When building a foundation for equal contributions, the goal is to create a model where **everyone can contribute**, but **not everyone is required to contribute**. This is especially important when preparing for decision making. 
- Anyone is welcome to voice their ideas and opinions, but if all voices are required to agree before implementation begins, operational efficiency will be sabotaged.

---
## Asynchronous workflows
- Conventional management philosophies may rely on intentional information silos or a "need-to-know-basis" model. This approach restricts transparency, with the goal of reducing misinformation.
- TeamOps flips this: information should be [public by default](https://about.gitlab.com/handbook/values/#transparency), with the goal of allowing maximum contribution. **"Public by default"** requires an organization to designate which information is [explicitly not public](https://about.gitlab.com/handbook/communication/confidentiality-levels/#not-public), creating a bias for transparency across all functions of a business.
- A traditional business problem is "how do we get the right information to the right people at the right time?" A TeamOps organization asks: "How do we create a system that allows everyone to access information and make contributions, regardless of role or function?"
- Here's an example: [Adjusting expensable entries in GitLab's expense report policy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104471)
- In many organizations, altering the expense report policy would require — at minimum — one meeting. In an organization powered by TeamOps, the proposal [begins as documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104471). 
- Robert M., a senior engineer at GitLab, identified the need to reverse a policy change. He documented his reasoning in a merge request, and notified the Procurement team and other stakeholders. This allowed feedback to be gathered and the appropriate owners of the policy to consider the changes at a time that worked best for them.
- Scaled across an organization, this meeting-free approach to making decisions enables more decisions to be made. This approach allows a more diverse array of perspectives to influence the decision, as there was no requirement to align 13 individuals to a single time slot on a given day for a synchronous meeting.

---
## Directly responsible individual (DRI)
- In TeamOps, it's crucial that each project or decision is assigned a single [directly responsible individual (DRI)](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) who is solely responsible for its success or failure. The DRI isn't responsible for doing all of the work: they are the ultimate decision-maker.
- This model [combinines the best of both hierarchical and consensus organizations](https://about.gitlab.com/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making). It helps avoid unclear expectations and delays from having too many people involved in a decision.
- Leaders must foster a culture where DRIs are empowered, able to [escalate to unblock](https://about.gitlab.com/handbook/values/#escalate-to-unblock), and willing to share their ideas in the open. This unlocks the team's highest potential. 
- A successful DRI should consult and collaborate with all teams and stakeholders and welcome input from a broad range of diverse perspectives as they form their thoughts.
- It's important to note that TeamOps still allows flexibility for team members to [disagree, commit, and disagree](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree), but it reduces the risk that disagreement or dissent will prevent a [bias for action](https://about.gitlab.com/handbook/values/#bias-for-action).
- Here's an example: [Learning & Development team member owns decisions related to her result metrics](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/24)
- A member of GitLab's Learning & Development team was responsible for developing mental health awareness content. Given that she was the one doing the work, and her result metric was the one impacted, she was given latitude to be the Directly Responsible Individual. This enabled her to make fast decisions about content type and structure, as opposed to waiting for a more senior person to sign off or appoint her as the lead for this piece of work.


---
## Give agency
- Efficient execution requires [agency to be given](https://about.gitlab.com/handbook/values/#give-agency) by default. 
- A critical component of workforce autonomy, agency empowers team members to independently and proactively make decisions without permission, review, or approval. 
- In other words, to self-govern as a manager of one It communicates that individuals are trusted to self-govern to accommodate their unique needs and design custom strategies to focus their time and attention on what they deem best.
- It’s unrealistic to conflate a value of agency to assume that all organizational decisions will be made completely independently. 
- Collaboration is still a critical value of TeamOps. But do all organizational decisions need collaboration? Strengthening agency in group dynamics can just start by removing rules or permissions for smaller operational components, like meeting attendance, personal task management systems, or working schedules.
- Agency is the antidote to micromanagement, which crushes execution, creativity, and retention. Instead, a shared reality that includes encouragement for each team member to design how and when they want to contribute fuels both individual and collective success.
- Here's an example: [Normalizing that it's OK to look away in video calls](https://about.gitlab.com/company/culture/all-remote/meetings/#9-its-ok-to-look-away)
- Giving agency begins in the most typical of places. Video calls are a natural part of day-to-day work for many knowledge workers, yet cultural expectations about presenteeism and attentiveness may restrict agency. 
- GitLab explicitly documents that it's OK to look away during meetings and that no one should be embarrassed to occasionally ask for something to be repeated. 
- By creating a culture where people are free to manage their own time and attention, they're able to direct energy on a minute-by-minute basis to execute. No one's path to execution looks the same. 
- It may involve breaks to connect with friends, taking a walk outside, or watching a recording of a meeting during a more suitable time.

---
## Key review meetings
- When and where are your team members able to make their contributions, and receive contributions from others? 
- In addition to standard communication channels, recurring opportunities dedicated exclusively to updates and knowledge sharing can help optimize awareness, questions, and feedback about an ongoing project. 
- These [Key Review Meetings](https://about.gitlab.com/handbook/key-review/) allow a functional group to stay updated on and discuss essential success measurements, such as: OKRs, KPIs, how the team is trending toward achieving goals, blocked tasks, new assignments, workstream changes, etc.
- In conventional organizations, this is apt to be a more informal conversation between a department head and their manager. 
- By broadening the audience of attendees for a Key Review Meeting to include the Chief Executive Officer (CEO), Chief Financial Officer (CFO), the function head, stable counterparts, and (optionally) all other executives and their direct reports, the pool of people who can contribute feedback, insights, and advice is expanded. 
- This forces the presenting department to be more mindful of execution, consider areas where they are falling short, and gather input for potential iterations toward progress.
- Similarly, cross-departmental conversations - known as [Group Conversations](https://about.gitlab.com/handbook/group-conversations/) within GitLab - can provide the same visibility and inclusion to other projects and teams, to help consider how a project may impact other OKRs throughout the organization. 
- These recurring meetings providing regular updates across all teams on a rotating schedule. 
- It's the same concept and content as key review meetings, with one major difference: all team members are invited! These meetings are designed to give the entire workforce context on what other teams outside of their own are focused on (and how they're executing). 
- Execution isn't solely about executing your goals, but also understanding what others are executing.
- Both key review meetings and cross-departmental conversations help keep operational pace, policies, and practices consistent throughout the organization, while also fostering a sense of inclusion.
- Here's an example: [GitLab User Experience (UX) department Key Review meeting](https://youtu.be/54LAX8UFU9s)
- GitLab User Experience (UX) department [livestreamed a Key Review meeting on GitLab Unfiltered](https://youtu.be/54LAX8UFU9s). A distinct element of these meetings is that no [presentations are allowed](https://about.gitlab.com/handbook/communication/#no-presenting-in-meetings). For context, each attendee was able to view a presentation prepared ahead of time, with a shared Google Doc agenda used to maintain an orderly and inclusive flow of questions and conversation. You'll notice that executives and their direct reports provide questions and suggestions throughout. There's a distinct conversation on usability beginning at the 13:51 mark where leads from various functions [contribute to improved execution on a 25-second lag recognized in the product](https://youtu.be/54LAX8UFU9s?t=831).

---
## Short toes
- An organization's speed of decision-making can be dramatically slowed if teams are concerned about "stepping on others' toes" when proposing an idea or contributing to work outside of their immediate job description. This is often fueled by a fear of conflict, which is one of the [five dysfunctions](https://about.gitlab.com/handbook/values/#five-dysfunctions) of a team.
- Adopting a TeamOps mentality means having [short toes](https://about.gitlab.com/handbook/values/#short-toes) and feeling comfortable with feedback, suggestions, and contributions to the work you "own". It also means speaking up when you see an opportunity for iteration. 
- Eliminating a territorial mindset allows for better [collaboration](https://about.gitlab.com/handbook/values/#collaboration), more [diversity of thought](https://about.gitlab.com/handbook/values/#seek-diverse-perspectives), and ultimately faster decisions.
- Here's an example: [Marketing team member contributing a proposal to improve a People function process](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/555)
- In a People Group Conversation at GitLab, the following question was asked: ***"What can we do from a company side to make sure people aren’t overworking?"*** At conventional organizations, people outside of the People Group may not risk ***"stepping on their toes"*** by proposing iterations and solutions. 
- At GitLab, [a proposal was offered](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/555) by a team member in Marketing. This proposal added an automated message within Slack to remind people to consider taking time off, and to add the note to their next manager 1:1 if they felt that they could not take PTO. Following a healthy discussion, the proposal was [added to GitLab's handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/37624) and implemented in its tool stack.

---
## Disagree, commit, and disagree
- In TeamOps, decisions are [two-way doors](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions), meaning they're easy to reverse. That's why a DRI should go ahead and make the decision without universal approval or consensus. The only time a decision should require a more thorough discussion first is when you can't reverse it or break it down into smaller, reversible components.
- "Disagree and commit” has become a commonly used phrase in some professional settings as a strategy to help prevent the consensus trap, which stalls decision velocity. It refers to a team’s ability to have disagreements while a decision is being made, but then committing to the change after a final conclusion has been confirmed.
- However, in an organization powered by TeamOps, a second "disagree" should be added to form ["Disagree, commit, and disagree."](https://about.gitlab.com/handbook/values/#disagree-commit-and-disagree) This mentality removes the time constraint of contributing ideas or feedback, and enables changes to be made without slowing down the pace of execution. 
- This is made possible by the value of iteration, which means that the team is only agreeing to one commitment at a time, so new contributions can easily be considered for the next iterative result. In the more conventional "disagree and commit" framework, execution is encouraged but contributions to future iterations are limited. By explicitly stating that team members are expected to execute (commit) while a decision stands, but are welcome to disagree, it invites everyone to constructively surface dissent and potential proposals for change.
- Increasing the flexibility of contributions and decision making requires a reframing of the conventional management mindset. Reverting work back to a previous state is a positive thing, because you're getting feedback more quickly and learning from it. Making a small change quickly prevents bigger (and slower) reverts and revisions in the future.
- Here's an example: [Require seniors to become maintainers](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106942)
- A GitLab merge request details a policy change to ["increase maintainers by requiring senior engineers to become a maintainer in at least one project/area."](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106942) 57 team members participated in discussion within the merge request itself, and the DRI for the change confirms in the description that ***"in a recent survey, 11% of respondents did not want to become a maintainer, and 35% disagreed that it should be a senior+ responsibility."*** Despite the disagreement, the iteration was merged and thus, a decision was made. Thanks to the operating principle **"Disagree, commit, and disagree,"** anyone is welcome to disagree with the change and influence the next iteration through constructive conversation with the DRI.

---
## Informal communication to build trust
- An intentional approach to [informal communication](https://about.gitlab.com/company/culture/all-remote/informal-communication/) is crucial in a fast-paced organization with a bias for [asynchronous workflows](https://about.gitlab.com/company/culture/all-remote/asynchronous) and [text-based communication](https://about.gitlab.com/company/culture/all-remote/effective-communication/). 
- Leaders should encourage team members to prioritize informal connections (e.g. coffee chats, social calls, special interest chat channels) and [get to know the people](https://about.gitlab.com/handbook/values/#get-to-know-each-other) behind the text. This builds trust, prevents conflict, and enables better communication during work-related interactions.
- Building this level of trust also helps enable DRIs to make faster decisions, as there's a foundation of confidence in the experience and judgment of others.
- Here's an example: [Sharing READMEs (personal operating manuals) to build trust with new team members](https://about.gitlab.com/handbook/engineering/readmes/)
- Two GitLab team members have never worked together before, so they set up a [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) and exchange [READMEs](https://about.gitlab.com/handbook/engineering/readmes/) prior to a new project starting. 
- They learn a lot about each other, their work styles, and their backgrounds in the 25-minute video call and the asynchronous README reviews prior. The project runs more smoothly because of their [shared trust](https://about.gitlab.com/handbook/leadership/building-trust/) beyond the transactional work interactions.

---
Reference : [Gitlab TeamOps](https://levelup.gitlab.com/learn/course/teamops)