+++
title = "Chef Cookbook Roles"
weight = 26400
chapter = false
pre = "<b>4. </b>"
+++

- Chef Cookbook
    - What is the Purpose of Chef Cookbook?
    - Structure of Chef Cookbook
    - Writing Chef Cookbook
    - Manage Chef Cookbooks
    - Do's and Dont's with Chef Cookbook
- Chef Role
    - What is the Purpose of Chef Role?
    - Create Chef Role
    - Manage Chef Roles
- Chef Node
    - Understanding Deployment on Chef Nodes
    - Using Docker Containers as Chef Nodes
    - Boot Chef Nodes
    - Trigger SSH-based Deployment on Chef Nodes
    - Bootstrap Workflow in Chef Node
    - Trigger Agent-based Deployment on Chef Nodes