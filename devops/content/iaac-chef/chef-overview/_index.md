+++
title = "Chef OVerview"
weight = 26100
chapter = false
pre = "<b>1. </b>"
+++

- Introduction to Chef
- Infrastructure Management
- What is Capacity Planning?
    - Major Capacity Planning Tools
- Understanding Capacity Planning
    - Calculating Percentile
- What is Provisioning?
    - Major Provisioning Tools
- What is Deployment?
    - Major Deployment Tools
    - Need for Automated Deployments
    - Need for Automated Deployments
- Deployment Matrix
    - App Service
    - Data Service
- What is Chef?
- History of Chef
