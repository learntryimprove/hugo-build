+++
title = "Swarm Overview"
weight = 9100
chapter = false
pre = "<b>1. </b>"
+++

- Introduction to Swarm
    - What is Docker?
    - Understanding Container Clustering & Orchestration
    - Benefits of Container Clustering & Orchestration
    - Major Container Clustering & Orchestration Tools
    - What is Swarm?
    - Understanding Swarm
    - History of Swarm
- Key Components in Swarm
    - Swarm Node 
    - Stack 
    - Service
- Overview of Swarm
    - Architecture of Swarm-managed Docker Containers
    - Swarm vs Kubernetes
- FAQ on Swarm