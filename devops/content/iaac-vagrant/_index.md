+++
title = "IaaC & Vagrant"
weight = 6006
chapter = false
+++

---
#### Standalone

- [Getting Vagrant Ready - Windows](/work-in-progress/)
- [Getting Vagrant Ready - Linux](/work-in-progress/)
- [Getting Ubuntu Ready](/work-in-progress/)
- [Getting CentOS Ready](/work-in-progress/)
- [Getting Nagios-Core Ready](/work-in-progress/)
- [Getting Nagios-XI Ready](/work-in-progress/)
- [Getting Nagios-LS Ready](/work-in-progress/)
- [Getting RabbitMQ Ready](/work-in-progress/)
- [Getting Nginx Ready](/work-in-progress/)
- [Getting Apache Ready](/work-in-progress/)
- [Getting MariaDB Ready](/work-in-progress/)
- [Getting PostgreSQL Ready](/work-in-progress/)
- [Getting MongoDB Ready](/work-in-progress/)
- [Getting Openshift Ready](/work-in-progress/)
- [Getting Docker Swarm Ready](/work-in-progress/)
- [Getting Kubernetes Ready](/work-in-progress/)
- [Getting ElasitcSearch Ready](/work-in-progress/)
- [Getting Prometheus Ready](/work-in-progress/)
- [Getting Grafana Ready](/work-in-progress/)
- [Getting Apache Kafka Ready](/work-in-progress/)
- [Getting Jenkins Ready](/work-in-progress/)
- [Getting Bamboo Ready](/work-in-progress/)

#### Clustered

- [Getting Nagios-Core Cluster Ready](/work-in-progress/)
- [Getting Nagios-XI Cluster Ready](/work-in-progress/)
- [Getting Nagios-LS Cluster Ready](/work-in-progress/)
- [Getting RabbitMQ Cluster Ready](/work-in-progress/)
- [Getting RabbitMQ Cluster Ready](/work-in-progress/)
- [Getting Nginx Cluster Ready](/work-in-progress/)
- [Getting Apache Cluster Ready](/work-in-progress/)
- [Getting MariaDB Cluster Ready](/work-in-progress/)
- [Getting PostgreSQL Cluster Ready](/work-in-progress/)
- [Getting MongoDB Cluster Ready](/work-in-progress/)
- [Getting Openshift Cluster Ready](/work-in-progress/)
- [Getting Docker Swarm Cluster Ready](/work-in-progress/)
- [Getting Kubernetes Cluster Ready](/work-in-progress/)
- [Getting ElasitcSearch Cluster Ready](/work-in-progress/)
- [Getting Prometheus Cluster Ready](/work-in-progress/)
- [Getting Apache Kafka Cluster Ready](/work-in-progress/)
- [Getting HAProxy Cluster Ready](/work-in-progress/)
- [Getting Jenkins Cluster Ready](/work-in-progress/)
- [Getting Bamboo Cluster Ready](/work-in-progress/)
