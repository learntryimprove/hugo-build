+++
title = "Terraform AWS"
weight = 22300
chapter = false
pre = "<b>3. </b>"
+++

- Terraform vs AWS CloudFormation
- Prerequisites for Terraform
    - Create AWS Account
    - Install AWS CLI 
- Setup Terraform
    - Install Terraform
    - Significance of Terraform Programs
- Terraform Provider
    - What is the Purpose of Terraform Provider?
    - Manage Terraform Providers
- Terraform Configuration
    - What is the Purpose of Terraform Configuration?
    - Structure of Terraform Configuration
    - Writing Terraform Configuration
    - Initialize Terraform Configuration
    - Generate Plan from Terraform Configuration
    - Apply Plan of Terraform Configuration
    - Manage Terraform Configurations
    - Do's and Dont's with Terraform Configuration
