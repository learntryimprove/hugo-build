+++
title = "Platform & Office365"
slug = "office365"
tags = ["tutorials","office","office365"]
weight = 8001
+++

---
Welcome, 

Have a great experience with tutorials around Office365.

#### Migrating from GSuite (Google Workspace) to Office365
- [Overview](/platform-office365/01-gsuite-to-office365/)
- [Preparing your Office365](#)
- [Preparing your GSuite (Google Workspace)](#)
- [Batch Migration on Office365](#)
- [Moving your Traffic from GSuite (Google Workspace) to Office365](#)
- [References](#)

#### Managing Office365 
- [Self Service](#)
- [MFA](#)

---
