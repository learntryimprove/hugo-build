+++
title = "DevOps & DevTestOps"
weight = 1006
chapter = false
+++

---
DevTestOps is a combination of DevOps and Continuous Testing. The process involves testing early, testing often, and testing throughout the software delivery pipeline. DevTestOps highlights the key role of the tester in the development of the product along with the Ops professionals.

![DevTestOps 01](/images/devops-devtestops/devtestops-0.png "DevTestOps 01 Icon")

![DevTestOps 02](/images/devops-devtestops/devtestops-1.png "DevTestOps 02 Icon")

---
Reference: https://testsigma.com/blog/what-is-devtestops-role-of-devtestops-in-continuous-testing/