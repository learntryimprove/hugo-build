+++
title = "Deep Learning"
weight = 30000
chapter = false
+++

---
### Introduction
- About this course
- General Information
- Learning Objectives
- Outlines
- Grading Scheme
- Copyrights and Trademarks
---
### Module 1 - Intro to Deep Learning
- Learning Objectives
- Introduction to Deep Learning (4:23)
- Deep Learning Pipeline (5:57)
---
### Module 2 - Hardware Accelerated Deep Learning
- Learning Objectives
- GPU-Accelerated Deep Learning Introduction (0:57)
- Parallelism in Deep Learning (3:15)
- Deep Learning with GPU vs CPU
- Single-GPU vs Multi-GPU Deep Learning
- Lab1 -Environment Setup
---
### Module 3 - Deep Learning in the Cloud
- Learning Objectives
- Hardware Accelerators (4:15)
- How does one use a GPU? (3:52)
- Deep Learning in the Cloud (4:56)
- Optional: Lab - How to run deep learning model in the Cloud
---
### Module 4 - Advanced Deep Learning
- Learning Objectives
- Distributed Deep Learning (1:31)
- Deep Learning without Coding: PowerAI vision (2:27)
- Object recognition with PowerAI vision (11:17)
---
### Module 5 - Deep Learning Project
- Learning Objectives
- What is the final project?
- How to do the project?
---
Reference : https://learning.edx.org/course/course-v1:IBM+DL0122EN+2T2020