+++
title = "Functional Testing"
weight = 1
chapter = false
+++

---
- [UAT](#uat)
- [Exploratory testing](#exploratory-testing)
- [Sanity Testing](#sanity-testing)
- [Regression Testing](#regression-testing)
- [Smoke Testing](#smoke-testing)
- [Unit Testing](#unit-testing)
- [Integration Testing](#integration-testing)

---
### UAT
User Acceptance Testing (UAT) is a type of testing performed by the end user or the client to verify/accept the software system before moving the software application to the production environment. UAT is done in the final phase of testing after functional, integration and system testing is done.

- READ [What is User Acceptance Testing (UAT)?](https://www.guru99.com/user-acceptance-testing.html)
- WATCH [How to plan your UAT](https://www.youtube.com/watch?v=AU8SV7091-s)

---
### Exploratory testing
Exploratory testing is evaluating a product by learning about it through exploration and experimentation, including to some degree: questioning, study, modeling, observation, inference, etc.

- READ [Exploratory Testing](https://www.satisfice.com/exploratory-testing)
- READ [Exploratory Testing 3.0](https://www.satisfice.com/blog/archives/1509)
- READ [History of Definitions of Exploratory Testing](https://www.satisfice.com/blog/archives/1504)

---
### Sanity Testing
Sanity testing is a kind of Software Testing performed after receiving a software build, with minor changes in code, or functionality, to ascertain that the bugs have been fixed and no further issues are introduced due to these changes. The goal is to determine that the proposed functionality works roughly as expected. If sanity test fails, the build is rejected to save the time and costs involved in a more rigorous testing.

- READ [Sanity Testing](https://www.geeksforgeeks.org/sanity-testing-software-testing/)

---
### Regression Testing
Regression Testing is a type of software testing to confirm that a recent program or code change has not adversely affected existing features. Regression testing is a black box testing technique. Test cases are re-executed to check the previous functionality of the application is working fine and that the new changes have not produced any bugs.

- READ [Regression Testing](https://www.javatpoint.com/regression-testing)
- READ [What is Regression Testing with Test Cases](https://www.guru99.com/regression-testing.html)

---
### Smoke Testing
Smoke Testing is a software testing process that determines whether the deployed software build is stable or not. Smoke testing is a confirmation for QA team to proceed with further software testing. It consists of a minimal set of tests run on each build to test software functionalities.

- READ [What is Smoke Testing?](https://www.guru99.com/smoke-testing.html)

---
### Unit Testing
Unit testing is where individual units (modules, functions/methods, routines, etc.) of software are tested to ensure their correctness. This low-level testing ensures smaller components are functionally sound while taking the burden off of higher-level tests. Generally, a developer writes these tests during the development process and they are run as automated tests.

- READ [Unit Testing Tutorial](https://www.guru99.com/unit-testing-guide.html)
- WATCH [What is Unit Testing?](https://youtu.be/3kzHmaeozDI)

---
### Integration Testing
Integration Testing is a type of testing where software modules are integrated logically and tested as a group. A typical software project consists of multiple software modules coded by different programmers. This testing level aims to expose defects in the interaction between these software modules when they are integrated. Integration Testing focuses on checking data communication amongst these modules.

- READ [Integration Testing Tutorial](https://www.guru99.com/integration-testing.html)

---
Reference: [https://roadmap.sh/qa](https://roadmap.sh/qa)